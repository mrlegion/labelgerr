﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dal.Mehdime.DbScope.Implementations;
using Dal.Repository;
using Domain.Services;
using Domain.Split;
using Infrastructure;

namespace AppConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            Console.WriteLine($@"Start operation on [ {start.ToLongTimeString()} ]{Environment.NewLine}");

            string pdf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Ярлыки ИП Герр.pdf");
            string title = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Ярлыки.txt");

            string db = ".\\db\\Sticker.db";
            if (File.Exists(db)) File.Delete(db);
            
            var pdfFile = PdfReadHelper.GetPageByteDictionary(pdf);
            var titles = FileReadHelper.ReadFileToList(title);

            if (pdfFile.Count != titles.Count())
            {
                Console.WriteLine(@"count titles and pages in pdf file not equal!");
                Console.ReadKey();
                return;
            }

            Console.WriteLine($@"Titles text file is load! Done! [ {DateTime.Now - start} ]");
            Console.WriteLine($@"Pdf file is load! Done! [ {DateTime.Now - start} ]{Environment.NewLine}");
            
            var service = new StickerService(new DbContextScopeFactory(),
                new StickerRepository(new AmbientDbContextLocator()));

            var stickers = new List<Sticker>();

            for (int i = 0; i < pdfFile.Count; i++)
            {
                stickers.Add(new Sticker(titles[i], pdfFile[i]));
                Console.WriteLine($@"Add to list: {titles[i]}");
            }

            Console.WriteLine($@"{Environment.NewLine}Add list to DB! [ {DateTime.Now - start} ]");
            service.AddRange(stickers);
            Console.WriteLine($@"Done! [ {DateTime.Now - start} ]{Environment.NewLine}");

            var list = service.GetAll();
            var pdfWriter = new PdfDocumentWriter();

            Console.WriteLine($@"Start saving files! [ {DateTime.Now - start} ]");
            pdfWriter.SaveToFiles(list);
            Console.WriteLine($@"Done! [ {DateTime.Now - start} ]{Environment.NewLine}");

            Console.WriteLine($@"Ending operation on [ {DateTime.Now.ToLongTimeString()} ]");
            Console.WriteLine($@"Time on work: [ {DateTime.Now - start} ]");

            Console.ReadKey();
        }
    }
}
