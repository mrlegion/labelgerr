﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using GalaSoft.MvvmLight.Threading;
using Meziantou.WpfFontAwesome;

namespace AppUI
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // set pro fonts
            //Dictionary<string, Uri> fonts = new Dictionary<string, Uri>()
            //{
            //    { "SolidPro",   new Uri(@"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resource\Fonts\fontawesome-pro-5.7.2-desktop\otfs\fa-solid-900.ttf", UriKind.Absolute) },
            //    { "RegularPro", new Uri(@"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resource\Fonts\fontawesome-pro-5.7.2-desktop\otfs\fa-regular-400.ttf", UriKind.Absolute) },
            //    { "LightPro",   new Uri(@"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resource\Fonts\fontawesome-pro-5.7.2-desktop\otfs\fa-light-300.ttf", UriKind.Absolute) },
            //    { "Brands",     new Uri(@"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resources\Fonts\fontawesome-pro-5.7.2-desktop\otfs\fa-brands-400.ttf", UriKind.Absolute) },
            //};

            //"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resources\Fonts\fontawesome-pro-5.7.2-desktop\otfs\Font Awesome 5 Brands-Regular-400.otf"
            //"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resources\Fonts\fontawesome-pro-5.7.2-desktop\otfs\Font Awesome 5 Pro-Light-300.otf"
            //"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resources\Fonts\fontawesome-pro-5.7.2-desktop\otfs\Font Awesome 5 Pro-Regular-400.otf"
            //"C:\Users\Alexander\Documents\Source\Repos\LabelGerr\AppUI\Resources\Fonts\fontawesome-pro-5.7.2-desktop\otfs\Font Awesome 5 Pro-Solid-900.otf"

            //FontAwesomeIcon.ProLightFontFamily = new FontFamily(fonts["LightPro"], "LightPro");
            //FontAwesomeIcon.ProRegularFontFamily = new FontFamily(fonts["RegularPro"], "RegularPro");
            //FontAwesomeIcon.ProSolidFontFamily = new FontFamily(fonts["SolidPro"], "SolidPro");
            //FontAwesomeIcon.ProBrandsFontFamily = new FontFamily(fonts["Brands"], "Brands");

            // init dispatcher
            DispatcherHelper.Initialize();
            base.OnStartup(e);
        }
    }
}
