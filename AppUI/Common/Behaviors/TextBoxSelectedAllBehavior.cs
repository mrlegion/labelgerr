﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace AppUI.Common.Behaviors
{
    public class TextBoxSelectedAllBehavior : Behavior<TextBox>
    {
        #region Public methods

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewMouseLeftButtonDown += OnPrewiewMouseLeftButtonDown;
            AssociatedObject.MouseDoubleClick += OnSelectAllText;
            AssociatedObject.GotKeyboardFocus += OnSelectAllText;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.PreviewMouseLeftButtonDown -= OnPrewiewMouseLeftButtonDown;
            AssociatedObject.MouseDoubleClick -= OnSelectAllText;
            AssociatedObject.GotKeyboardFocus -= OnSelectAllText;
        }

        #endregion

        #region Private methods

        private void OnSelectAllText(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is TextBox textBox)
                textBox.SelectAll();
        }

        private void OnPrewiewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject parent = e.OriginalSource as UIElement;
            while (parent != null && !(parent is TextBox))
                parent = VisualTreeHelper.GetParent(parent);

            if (parent != null)
            {
                var textBox = (TextBox)parent;
                if (!textBox.IsKeyboardFocusWithin)
                {
                    textBox.Focus();
                    e.Handled = true;
                }
            }
        }

        #endregion
    }
}