﻿using System.Collections.Generic;

namespace AppUI.Common.Helpers
{
    public class CountStickerHelper
    {
        public static int[] GetCountArray()
        {
            var list = new List<int>();
            for (int i = 1; i <= 20; i++)
                list.Add(i * 50);
            return list.ToArray();
        }
    }
}