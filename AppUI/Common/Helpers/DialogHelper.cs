﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using AppUI.UserControls.Dialogs;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Threading;
using MaterialDesignThemes.Wpf;

namespace AppUI.Common.Helpers
{
    public class DialogHelper
    {
        /// <summary>
        /// Show dialog window when loaded data in memory
        /// </summary>
        /// <param name="callback">Loaded action</param>
        /// <param name="message">Message in dialog</param>
        /// <param name="identifier">Identifier dialog tag in XAML</param>
        public static void ShowDialog(Action callback, string message, string identifier)
        {
            LoadDialogView content = ServiceLocator.Current.GetInstance<LoadDialogView>();
            ((LoadDialogViewModel) content.DataContext).Message = message;

            DialogHost.Show(content, identifier, delegate(object sender, DialogOpenedEventArgs args)
            {
                ThreadPool.QueueUserWorkItem(o =>
                {
                    callback.Invoke();
                    DispatcherHelper.CheckBeginInvokeOnUI(() => args.Session.Close(false));
                });
            });
        }

        /// <summary>
        /// Show dialog window when loaded data in memory with identifier default (on properties app)
        /// </summary>
        /// <param name="callback">Loaded action</param>
        /// <param name="message">Message in dialog</param>
        public static void ShowDialog(Action callback, string message)
        {
            string identifier = Properties.Settings.Default.Identifier;
            if (identifier == null) throw new ArgumentNullException(nameof(identifier), @"Identifier for dialog host cannot be null");
            ShowDialog(callback, message, identifier);
        }

        public static async Task<object> ShowDialog<TView, TModel, TEntity>(TEntity item, string title = "")
            where TModel : DialogViewModelBase<TEntity>
            where TView : UserControl
            where TEntity : class
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            var content = ServiceLocator.Current.GetInstance<TView>();
            ((TModel)content.DataContext).Init(item, title);

            return await DialogHost.Show(content, Properties.Settings.Default.Identifier); 
        }

        public static async Task<object> ShowDialog<TView, TModel>(string identifier)
            where TView : UserControl
        {
            var content = ServiceLocator.Current.GetInstance<TView>();

            return await DialogHost.Show(content, identifier);
        }

        public static async Task<object> ShowDialog<TView, TModel>()
            where TView : UserControl
        {
            return await ShowDialog<TView, TModel>(Properties.Settings.Default.Identifier);
        }

        public static void ShowInformerDialog(string message, PackIconKind icon)
        {
            ShowInformerDialog(message, icon, Properties.Settings.Default.Identifier);
        }

        public static async void ShowInformerDialog(string message, PackIconKind icon, string identifier)
        {
            var content = ServiceLocator.Current.GetInstance<DialogView>();
            ((DialogViewModel)content.DataContext).Initialize(message, icon);
            await DialogHost.Show(content, identifier);
        }

        public static async Task<bool> ShowQuestionDialog(string message, PackIconKind icon)
        {
            return await ShowQuestionDialog(message, icon, Properties.Settings.Default.Identifier);
        }

        public static async Task<bool> ShowQuestionDialog(string message, PackIconKind icon, string identifier)
        {
            var content = ServiceLocator.Current.GetInstance<DialogQuestionView>();
            ((DialogViewModel)content.DataContext).Initialize(message, icon);
            var result = await DialogHost.Show(content, identifier);
            if (result is bool request) return request;
            return false;
        }
    }
}