﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight;

namespace AppUI.Common.Services.NavigationService
{
    public class FrameNavigationService : ViewModelBase, IFrameNavigationService
    {
        private readonly Dictionary<string, Uri> _pageByKey;
        private readonly Stack<string> _historic;

        private string _currentPageKey;

        public FrameNavigationService(string frameName)
        {
            FrameName = frameName;
            _pageByKey = new Dictionary<string, Uri>();
            _historic = new Stack<string>();
        }

        public string CurrentPageKey
        {
            get { return _currentPageKey; }
            set { Set(nameof(CurrentPageKey), ref _currentPageKey, value); }
        }

        public object Parameters { get; set; }

        public string FrameName { get; set; }

        public bool CanGoBack
        {
            get { return _historic.Count > 1; }
        }

        public void GoBack()
        {
            if (CanGoBack)
            {
                _historic.Pop();
                NavigateTo(_historic.Pop(), null);
            }
        }

        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null);
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            lock (_pageByKey)
            {
                if (!_pageByKey.ContainsKey(pageKey)) throw new ArgumentException($@"No such page: {pageKey}", nameof(pageKey));
                if (GetDescendantFromName(Application.Current.MainWindow, FrameName) is Frame frame)
                    frame.Source = _pageByKey[pageKey];
                Parameters = parameter;
                _historic.Push(pageKey);
                CurrentPageKey = pageKey;
            }
        }
        
        public void Configuration(string key, Uri pageType)
        {
            lock (_pageByKey)
                if (_pageByKey.ContainsKey(key))
                    _pageByKey[key] = pageType;
                else _pageByKey.Add(key, pageType);
        }

        private FrameworkElement GetDescendantFromName(DependencyObject parent, string name)
        {
            var count = VisualTreeHelper.GetChildrenCount(parent);
            if (count < 1) return null;
            for (int i = 0; i < count; i++)
                if (VisualTreeHelper.GetChild(parent, i) is FrameworkElement frameworkElement)
                {
                    if (frameworkElement.Name == name)
                        return frameworkElement;
                    frameworkElement = GetDescendantFromName(frameworkElement, name);
                    if (frameworkElement != null)
                        return frameworkElement;
                }
            return null;
        }
    }
}