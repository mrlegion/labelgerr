﻿using System;
using GalaSoft.MvvmLight.Views;

namespace AppUI.Common.Services.NavigationService
{
    public interface IFrameNavigationService : INavigationService
    {
        object Parameters { get; set; }
        string FrameName { get; set; }
        bool CanGoBack { get; }

        void Configuration(string key, Uri pageType);
    }
}