﻿using System;
using AppUI.Common.Services.NavigationService;
using Autofac;

namespace AppUI.Common.Services
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // navigation service
            RegisterNavigation(builder);
        }

        private void RegisterNavigation(ContainerBuilder builder)
        {
            IFrameNavigationService service = new FrameNavigationService(Properties.Settings.Default.MainFrameNavigation);

            // registration navigation
            service.Configuration("library", new Uri("..\\..\\Views\\LibraryView.xaml", UriKind.Relative));
            service.Configuration("select", new Uri("..\\..\\Views\\SelectView.xaml", UriKind.Relative));
            service.Configuration("addToLibrary", new Uri("..\\..\\Views\\AddToLibraryView.xaml", UriKind.Relative));
            service.Configuration("generate", new Uri("..\\..\\Views\\GenerateStickerView.xaml", UriKind.Relative));

            // registration in builder
            builder.Register(c => service).As<IFrameNavigationService>();
        }
    }
}