﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;
using AppUI.Common.Helpers;
using CommonServiceLocator;
using Domain.Services;
using Domain.Split;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AppUI.UserControls.AddToLibrary
{
    public class AddManyViewModel : ViewModelBase
    {
        #region Fields

        private string _pdfFile;
        private string _titleFile;
        private Brush _defaultBrush;
        private int? _countPages;
        private RelayCommand<Button> _selectTitleCommand;
        private RelayCommand<Button> _selectPdfCommand;
        private RelayCommand<UserControl> _clearCommand;
        private RelayCommand _addCommand;

        #endregion

        #region Constructor

        public AddManyViewModel()
        {
            CountPages = null;
        }

        #endregion

        #region Properties

        public int? CountPages
        {
            get { return _countPages; }
            set { Set(nameof(CountPages), ref _countPages, value); }
        }

        public RelayCommand<Button> SelectTitleCommand
        {
            get
            {
                return _selectTitleCommand ?? (_selectTitleCommand = new RelayCommand<Button>((button) =>
                {
                    var dialog = new CommonOpenFileDialog()
                    {
                        IsFolderPicker = false,
                        Multiselect = false,
                        Title = "Select txt file with naming for stickers",
                        Filters = { new CommonFileDialogFilter("Title txt file", "txt") },
                    };

                    if (dialog.ShowDialog() != CommonFileDialogResult.Ok) return;

                    if (_defaultBrush == null)
                        _defaultBrush = button.Foreground;

                    button.Foreground = Brushes.Green;
                    button.Content = "Selected";

                    _titleFile = dialog.FileName;
                }));
            }
        }

        public RelayCommand<Button> SelectPdfCommand
        {
            get
            {
                return _selectPdfCommand ?? (_selectPdfCommand = new RelayCommand<Button>((button) =>
                {
                    var dialog = new CommonOpenFileDialog()
                    {
                        IsFolderPicker = false,
                        Multiselect = false,
                        Title = "Select pdf file with content of stickers",
                        Filters = { new CommonFileDialogFilter("Stickers pdf file", "pdf") },
                    };

                    if (dialog.ShowDialog() != CommonFileDialogResult.Ok) return;

                    if (_defaultBrush == null)
                        _defaultBrush = button.Foreground;

                    button.Foreground = Brushes.Green;
                    button.Content = "Selected";

                    _pdfFile = dialog.FileName;
                    CountPages = PdfReadHelper.GetNumberOfPages(_pdfFile);
                }));
            }
        }

        public RelayCommand<UserControl> ClearCommand
        {
            get
            {
                return _clearCommand ?? (_clearCommand = new RelayCommand<UserControl>((uc) =>
                {
                    if (uc == null) throw new ArgumentNullException(nameof(uc));

                    // reset variable
                    _pdfFile = default(string);
                    _titleFile = default(string);
                    CountPages = null;

                    // reset button
                    var pdfButton = uc.FindName("PdfFileButton") as Button;
                    var titleButton = uc.FindName("TitleButton") as Button;

                    if (pdfButton == null || titleButton == null)
                        throw new ArgumentNullException();

                    pdfButton.Foreground = _defaultBrush;
                    titleButton.Foreground = _defaultBrush;

                    pdfButton.Content = titleButton.Content = "Select";
                }));
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new RelayCommand(() =>
                {
                    if (!File.Exists(_titleFile)) throw new ArgumentNullException(nameof(_titleFile));
                    if (!File.Exists(_pdfFile)) throw new ArgumentNullException(nameof(_pdfFile));

                    DialogHelper.ShowDialog(() =>
                    {
                        var stickers = StickerFactory.GetStickers(_titleFile, _pdfFile);
                        var service = ServiceLocator.Current.GetInstance<StickerService>();
                        service.AddRange(stickers);
                    }, $"Load data to Database!{Environment.NewLine}Wait few moments...");
                }));
            }
        }

        #endregion
    }
}