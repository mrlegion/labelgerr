﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using AppUI.Common.Helpers;
using CommonServiceLocator;
using Domain.Services;
using Domain.Split;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Infrastructure;
using MaterialDesignThemes.Wpf;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AppUI.UserControls.AddToLibrary
{
    public class AddSingleViewModel : ViewModelBase
    {
        #region Fields

        private Brush _defaultBrush;
        private string _pdfFile;
        private string _name;
        private string _numberPage;
        private bool _isSelectPage;
        private RelayCommand<Button> _selectPdfCommand;
        private RelayCommand<UserControl> _clearCommand;
        private RelayCommand _addCommand;

        #endregion

        #region Properties

        public string Name
        {
            get { return _name; }
            set { Set(nameof(Name), ref _name, value); }
        }

        public string NumberPage
        {
            get { return _numberPage; }
            set { Set(nameof(NumberPage), ref _numberPage, value); }
        }

        public bool IsSelectPage
        {
            get { return _isSelectPage; }
            set { Set(nameof(IsSelectPage), ref _isSelectPage, value); }
        }

        public RelayCommand<Button> SelectPdfCommand
        {
            get
            {
                return _selectPdfCommand ?? (_selectPdfCommand = new RelayCommand<Button>((button) =>
                {
                    var dialog = new CommonOpenFileDialog()
                    {
                        IsFolderPicker = false,
                        Multiselect = false,
                        Title = "Select pdf file with content of stickers",
                        Filters = { new CommonFileDialogFilter("Stickers pdf file", "pdf") },
                    };

                    if (dialog.ShowDialog() != CommonFileDialogResult.Ok) return;

                    if (_defaultBrush == null)
                        _defaultBrush = button.Foreground;

                    button.Foreground = Brushes.Green;
                    button.Content = "Selected";

                    _pdfFile = dialog.FileName;
                }));
            }
        }

        public RelayCommand<UserControl> ClearCommand
        {
            get
            {
                return _clearCommand ?? (_clearCommand = new RelayCommand<UserControl>((uc) =>
                {
                    if (uc == null) throw new ArgumentNullException(nameof(uc));

                    // reset variable
                    _pdfFile = default(string);
                    Name = default(string);
                    NumberPage = default(string);
                    IsSelectPage = false;

                    // reset button
                    var pdfButton = uc.FindName("PdfFileButton") as Button;
                    if (pdfButton == null) throw new ArgumentNullException(nameof(pdfButton));
                    pdfButton.Foreground = _defaultBrush;
                    pdfButton.Content = "Select";
                }));
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new RelayCommand(() =>
                {
                    var sticker = new Sticker
                    {
                        Name = Name,
                        File = IsSelectPage
                                ? PdfReadHelper.GetPageBytesByNumber(_pdfFile, int.Parse(NumberPage))
                                : PdfReadHelper.GetPageBytes(_pdfFile)
                    };

                    StickerService service = ServiceLocator.Current.GetInstance<StickerService>();
                    service.Add(sticker);

                    DialogHelper.ShowInformerDialog("New sticker has success added to Database", PackIconKind.InfoOutline);
                }));
            }
        }

        #endregion
    }
}