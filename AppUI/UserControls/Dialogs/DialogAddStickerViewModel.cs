﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using CommonServiceLocator;
using Domain.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using Infrastructure;

namespace AppUI.UserControls.Dialogs
{
    public class DialogAddStickerViewModel : ViewModelBase
    {
        private ObservableCollection<Sticker> _items;
        private IList _selectedItems;
        private string _searchName;
        private string _title;

        private ICollectionView _stickers;

        public DialogAddStickerViewModel()
        {
            Title = "Select stickers for add in group";
            SelectedItems = new List<Sticker>();

            // init sort collection
            CViewSource = new CollectionViewSource();
            CViewSource.Filter += NamingFilter;

            OnFillList();
        }

        private CollectionViewSource CViewSource { get; }

        public ICollectionView Stickers
        {
            get { return _stickers; }
            set { Set(nameof(Stickers), ref _stickers, value); }
        }

        public string Title
        {
            get { return _title; }
            set { Set(nameof(Title), ref _title, value); }
        }

        public string SearchName
        {
            get { return _searchName; }
            set
            {
                Set(nameof(SearchName), ref _searchName, value);
                CViewSource.View.Refresh();
            }
        }

        public IList SelectedItems
        {
            get { return _selectedItems; }
            set { Set(nameof(SelectedItems), ref _selectedItems, value); }
        }

        private void OnFillList()
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                var service = ServiceLocator.Current.GetInstance<StickerService>();
                var all = service.GetAll();
                _items = new ObservableCollection<Sticker>(all);
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    CViewSource.Source = _items;
                    Stickers = CViewSource.View;
                });
            });

        }

        private void NamingFilter(object sender, FilterEventArgs args)
        {
            if (args.Item is Sticker sticker)
                if (string.IsNullOrWhiteSpace(SearchName) || SearchName.Length == 0)
                    args.Accepted = true;
                else
                    args.Accepted = (sticker.Name.IndexOf(SearchName, StringComparison.OrdinalIgnoreCase) >= 0);
        }
    }
}