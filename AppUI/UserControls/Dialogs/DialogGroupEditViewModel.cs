﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppUI.Common.Helpers;
using GalaSoft.MvvmLight.Command;
using Infrastructure;
using MaterialDesignThemes.Wpf;

namespace AppUI.UserControls.Dialogs
{
    public class DialogGroupEditViewModel : DialogViewModelBase<Group>
    {
        private RelayCommand<Sticker> _deleteStickerCommand;

        public RelayCommand<Sticker> DeleteStickerCommand
        {
            get
            {
                return _deleteStickerCommand ?? (_deleteStickerCommand = new RelayCommand<Sticker>(async (sticker) =>
                {
                    if (sticker == null)
                        DialogHelper.ShowInformerDialog("Not found selected sticker!" + Environment.NewLine + "Please check your choose!", 
                            PackIconKind.QuestionMarkCircle, Properties.Settings.Default.SecondDialogHost);

                    bool request = await DialogHelper.ShowQuestionDialog(
                        "You realy won delete select sticker on Group?\r\n" + sticker?.Name,
                        PackIconKind.QuestionMarkCircle, Properties.Settings.Default.SecondDialogHost);

                    if (request)
                    {
                        var temp = Entity.Stickers.ToList();
                        temp.Remove(sticker);
                        Entity.Stickers = temp;
                    }
                }));
            }
        }

        private RelayCommand _addStickerCommand;

        public RelayCommand AddStickerCommand
        {
            get
            {
                return _addStickerCommand ?? (_addStickerCommand = new RelayCommand(async () =>
                           {
                               object request = await DialogHelper.ShowDialog<DialogAddStickerView, DialogAddStickerViewModel>(Properties.Settings.Default.SecondDialogHost);
                               if (request == null) return;
                               if (request is List<Sticker> stickers)
                               {
                                   var temp = Entity.Stickers.ToList();
                                   temp.AddRange(stickers);
                                   Entity.Stickers = temp;
                               }
                           }));
            }
        }
    }
}