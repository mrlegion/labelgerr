﻿using AppUI.Common.Helpers;
using GalaSoft.MvvmLight.Command;
using Infrastructure;
using MaterialDesignThemes.Wpf;


namespace AppUI.UserControls.Dialogs
{
    public class DialogGroupViewModel : DialogViewModelBase<Group>
    {
        private RelayCommand _viewStickerCommand;

        public RelayCommand ViewStickerCommand
        {
            get
            {
                return _viewStickerCommand ?? (_viewStickerCommand = new RelayCommand( () =>
                           {
                               DialogHelper.ShowInformerDialog("Test double dialog in app",
                                   PackIconKind.Information, "SecondDialogHost");
                           }));
            }
        }
    }
}