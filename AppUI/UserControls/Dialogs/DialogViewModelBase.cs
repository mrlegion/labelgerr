﻿using System;
using GalaSoft.MvvmLight;

namespace AppUI.UserControls.Dialogs
{
    public class DialogViewModelBase<T> : ViewModelBase
    {
        private string _title;

        public string Title
        {
            get { return _title; }
            set { Set(nameof(Title), ref _title, value); }
        }

        private T _entity;

        public T Entity
        {
            get { return _entity; }
            set { Set(nameof(Entity), ref _entity, value); }
        }

        public virtual void Init(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity), @"Entity has null");
            Entity = entity;
        }

        public virtual void Init(T entity, string title)
        {
            Init(entity);
            Title = title;
        }
    }
}