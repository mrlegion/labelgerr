﻿using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight.Command;
using Infrastructure;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AppUI.UserControls.Dialogs
{
    public class EditDialogViewModel : DialogViewModelBase<Sticker>
    {
        private string _buttonTitle;

        public string ButtonTitle
        {
            get { return _buttonTitle; }
            set { Set(nameof(ButtonTitle), ref _buttonTitle, value); }
        }

        private string _path;

        public string Path
        {
            get { return _path; }
            set { Set(nameof(Path), ref _path, value); }
        }

        public EditDialogViewModel()
        {
            ButtonTitle = "Select new PDF file for this sticker";
        }

        private RelayCommand<Button> _selectFileCommand;

        public RelayCommand<Button> SelectFileCommand
        {
            get
            {
                return _selectFileCommand ?? (_selectFileCommand = new RelayCommand<Button>((b) =>
                {
                    var dialog = new CommonOpenFileDialog()
                    {
                        Title = "Select new PDF file for sticker",
                        IsFolderPicker = false,
                        Multiselect = false,
                        Filters = { new CommonFileDialogFilter("Pdf file", "pdf") }
                    };

                    if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        ButtonTitle = "Selected new file";
                        
                        // set new color for button
                        b.Background = Brushes.ForestGreen;
                        b.BorderBrush = Brushes.ForestGreen;

                        // set path for select file
                        Path = dialog.FileName;
                    }
                }));
            }
        }
    }
}