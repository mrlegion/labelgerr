﻿using AppUI.UserControls.AddToLibrary;
using AppUI.UserControls.Dialogs;
using Autofac;

namespace AppUI.UserControls
{
    public class UserControlModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // register dialogs
            RegisterDialogs(builder);

            // add to library
            RegisterLibraryHelper(builder);
        }

        private void RegisterLibraryHelper(ContainerBuilder builder)
        {
            builder.RegisterType<AddManyView>();
            builder.RegisterType<AddManyViewModel>();
            builder.RegisterType<AddSingleView>();
            builder.RegisterType<AddSingleViewModel>();
        }

        private void RegisterDialogs(ContainerBuilder builder)
        {
            builder.RegisterType<EditDialogView>();
            builder.RegisterType<LoadDialogView>();
            builder.RegisterType<DialogView>();
            builder.RegisterType<DialogQuestionView>();
            builder.RegisterType<EditDialogViewModel>();
            builder.RegisterType<LoadDialogViewModel>();
            builder.RegisterType<DialogViewModel>();
            builder.RegisterType<DialogGroupView>();
            builder.RegisterType<DialogGroupViewModel>();
            builder.RegisterType<DialogGroupEditViewModel>();
            builder.RegisterType<DialogGroupEditView>();
            builder.RegisterType<DialogAddStickerView>();
            builder.RegisterType<DialogAddStickerViewModel>();
        }
    }
}