﻿using AppUI.Common.Services.NavigationService;
using GalaSoft.MvvmLight;

namespace AppUI.ViewModels
{
    public class AddToLibraryViewModel : ViewModelCustom
    {
        public AddToLibraryViewModel(IFrameNavigationService navigationService) : base(navigationService)
        {
        }

    }
}