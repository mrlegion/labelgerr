﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using AppUI.Common.Helpers;
using AppUI.UserControls.Dialogs;
using CommonServiceLocator;
using Domain.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using Infrastructure;
using MaterialDesignThemes.Wpf;

namespace AppUI.ViewModels
{
    public class GenerateStickerViewModel : ViewModelBase
    {
        #region Fields

        private ObservableCollection<Sticker> _items;
        private ICollectionView _filteredItems;
        private string _searchName;
        private IList _selectedItems;
        private int[] _countList;
        private int _count;
        private bool _useShuffle;
        private bool _toPrintPage;
        private ObservableCollection<Group> _groups;
        private RelayCommand _debugSelectedItemCommand;
        private RelayCommand<object> _addGroupCommand;
        private RelayCommand<object> _viewGroupCommand;
        private RelayCommand<object> _deleteGroupCommand;
        private RelayCommand<object> _editGroupCommand;

        private RelayCommand<object> _clearCommand;

        #endregion

        #region Construct

        public GenerateStickerViewModel()
        {
            // init selected items collection
            SelectedItems = new List<Sticker>();
            CountList = CountStickerHelper.GetCountArray();
            Count = 100;
            Groups = new ObservableCollection<Group>();

            // init sort collection
            CViewSource = new CollectionViewSource();
            CViewSource.Filter += NamingFilter;

            // fill list stickers
            DialogHelper.ShowDialog(OnFillList, "Load data" + Environment.NewLine + "Please wait a moment...");
        }

        #endregion

        #region Properties

        private CollectionViewSource CViewSource { get; }

        public ICollectionView FilteredItems
        {
            get { return _filteredItems; }
            set { Set(nameof(FilteredItems), ref _filteredItems, value); }
        }

        public string SearchName
        {
            get { return _searchName; }
            set
            {
                Set(nameof(SearchName), ref _searchName, value);
                CViewSource.View.Refresh();
            }
        }

        public bool UseShuffle
        {
            get { return _useShuffle; }
            set { Set(nameof(UseShuffle), ref _useShuffle, value); }
        }

        public bool ToPrintPage
        {
            get { return _toPrintPage; }
            set { Set(nameof(ToPrintPage), ref _toPrintPage, value); }
        }

        public IList SelectedItems
        {
            get { return _selectedItems; }
            set { Set(nameof(SelectedItems), ref _selectedItems, value); }
        }

        public int[] CountList
        {
            get { return _countList; }
            set { Set(nameof(CountList), ref _countList, value); }
        }

        public int Count
        {
            get { return _count; }
            set { Set(nameof(Count), ref _count, value); }
        }

        public ObservableCollection<Group> Groups
        {
            get { return _groups; }
            set { Set(nameof(Groups), ref _groups, value); }
        }

        public RelayCommand DebugSelectedItemCommand
        {
            get
            {
                return _debugSelectedItemCommand ?? (_debugSelectedItemCommand = new RelayCommand(() =>
                {
                    if (SelectedItems == null) throw new ArgumentNullException(nameof(SelectedItems));

                    List<Sticker> list = SelectedItems.Cast<Sticker>().ToList();

                    var sb = new StringBuilder();
                    foreach (Sticker sticker in list)
                        sb.Append(sticker.Name + Environment.NewLine);

                    DialogHelper.ShowInformerDialog(sb.ToString(), PackIconKind.Information);
                }));
            }
        }

        public RelayCommand<object> AddGroupCommand
        {
            get
            {
                return _addGroupCommand ?? (_addGroupCommand = new RelayCommand<object>(execute: (o) =>
                {
                    BaseOnCommand<ListView>(
                        async view => { await AddGroupCommandHandler(view); }, o);
                }));
            }
        }

        public RelayCommand<object> ViewGroupCommand
        {
            get
            {
                return _viewGroupCommand ?? (_viewGroupCommand = new RelayCommand<object>(async (o) => await ViewGroupCommandHandler(o)));
            }
        }

        public RelayCommand<object> DeleteGroupCommand
        {
            get
            {
                return _deleteGroupCommand ?? (_deleteGroupCommand = new RelayCommand<object>(async (o) => await DeleteGroupCommandHandler(o)));
            }
        }

        public RelayCommand<object> EditGroupCommand
        {
            get
            {
                return _editGroupCommand ?? (_editGroupCommand = new RelayCommand<object>(async (o) => await EditGroupCommandHandler(o)));
            }
        }

        public RelayCommand<object> ClearCommand
        {
            get { return _clearCommand ?? (_clearCommand = new RelayCommand<object>(o => BaseOnCommand<ListView>(
                              async view=>
                             {
                                 bool request = await DialogHelper.ShowQuestionDialog(
                                     "You realy want delete all information?", PackIconKind.Delete);
                                 if (request)
                                 {
                                     view.UnselectAll();
                                     SelectedItems.Clear();
                                     SearchName = string.Empty;
                                     Count = default(int);
                                     Groups.Clear();
                                 }
                             }, o)));
            }
        }

        #endregion

        #region Private methods

        private static async Task ViewGroupCommandHandler(object o)
        {
            if (o == null)
            {
                DialogHelper.ShowInformerDialog("Not found selected groups", PackIconKind.Error);
                return;
            }

            if (o is Group group)
                await DialogHelper.ShowDialog<DialogGroupView, DialogGroupViewModel, Group>(@group, "Info about " + group.Name);
        }

        private async Task DeleteGroupCommandHandler(object o)
        {
            if (o == null)
            {
                DialogHelper.ShowInformerDialog("Not found selected groups", PackIconKind.Error);
                return;
            }

            if (o is Group group)
            {
                bool request = await DialogHelper.ShowQuestionDialog(
                    "You realy wont delete select group?" + Environment.NewLine + group.Name,
                    PackIconKind.Delete);
                if (request) Groups.Remove(group);
            }
        }

        private async Task EditGroupCommandHandler(object o)
        {
            if (o == null)
            {
                DialogHelper.ShowInformerDialog("Not found selected groups", PackIconKind.Error);
                return;
            }

            if (o is Group group)
            {
                var sendGroup = new Group() {Name = @group.Name, Count = @group.Count, Stickers = @group.Stickers};
                var request =
                    await DialogHelper.ShowDialog<DialogGroupEditView, DialogGroupEditViewModel, Group>(sendGroup,
                        "Edit group " + @group.Name);
                if (request == null) return;
                if (request is Group newGroup)
                {
                    var index = Groups.IndexOf(@group);
                    Groups[index] = newGroup;
                }
            }
        }

        private void BaseOnCommand<T>(Action<T> predicate, object o)
        {
            if (o == null) DialogHelper.ShowInformerDialog($"Cannot found send type [{typeof(T).FullName}] in command!", PackIconKind.Error);
            else if (o is T item) predicate.Invoke(item);
            else DialogHelper.ShowInformerDialog("Error in command!", PackIconKind.Error);
        }

        private void OnFillList()
        {
            var service = ServiceLocator.Current.GetInstance<StickerService>();
            var all = service.GetAll();
            _items = new ObservableCollection<Sticker>(all);
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                CViewSource.Source = _items;
                FilteredItems = CViewSource.View;
            });
        }

        private void NamingFilter(object sender, FilterEventArgs args)
        {
            if (args.Item is Sticker sticker)
                if (string.IsNullOrWhiteSpace(SearchName) || SearchName.Length == 0)
                    args.Accepted = true;
                else
                    args.Accepted = (sticker.Name.IndexOf(SearchName, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private async Task AddGroupCommandHandler(ListView listView)
        {
            // todo: maybe, fuck this check to ass and block adding group to Groups collection if count for group contains in collection
            var group = new Group($"Group by {Count}", Count, new List<Sticker>(SelectedItems.Cast<Sticker>()));
            if (Groups.Contains(group))
            {
                bool request = await DialogHelper.ShowQuestionDialog(
                    "Group with this count is exist!\r\nYou can adding selected sticker to this group.\r\nAdding to exist group?",
                    PackIconKind.QuestionMark);
                if (request)
                {
                    // get index contains group
                    int index = Groups.IndexOf(group);
                    // create and fill two collection for check contains dublicates stickers in group
                    var exist = new List<Sticker>();
                    var notExist = new List<Sticker>();
                    foreach (Sticker sticker in SelectedItems.Cast<Sticker>())
                        if (((List<Sticker>) Groups[index].Stickers).Contains(sticker)) exist.Add(sticker);
                        else notExist.Add(sticker);

                    // check equals and notequals collection
                    if (exist.Any())
                    {
                        if (!notExist.Any())
                        {
                            DialogHelper.ShowInformerDialog(
                                "In selected sticker not found new selected stickers.\r\nPlease check you selection, maybe you need create new group?",
                                    PackIconKind.Error);
                            return;
                        }

                        // show info about contains stickers
                        DialogHelper.ShowInformerDialog("Found in selected stickers existing elements:\r\n\r\n" + StickerToString(exist),
                            PackIconKind.InfoOutline, Properties.Settings.Default.SecondDialogHost);

                        // adding new stickers to collections
                        ((List<Sticker>)Groups[index].Stickers).AddRange(notExist);
                        DialogHelper.ShowInformerDialog("Adding to exist " + Groups[index].Name + "\r\n" + StickerToString(notExist), PackIconKind.InfoOutline);
                        listView.UnselectAll();
                    }
                    else DialogHelper.ShowInformerDialog("Error to adding group!", PackIconKind.Error);
                }
            }
            else
            {
                Groups.Add(group);
                listView.UnselectAll();
            }
        }

        private string StickerToString(IEnumerable<Sticker> stickers)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Sticker sticker in stickers)
                sb.Append(sticker.Name + "\r\n");
            return sb.ToString();
        }

        #endregion
    }
}