﻿using System;
using System.Collections.Generic;
using System.Threading;
using AppUI.Common.Helpers;
using AppUI.Common.Services.NavigationService;
using AppUI.UserControls.Dialogs;
using CommonServiceLocator;
using Domain.Services;
using Domain.Split;
using GalaSoft.MvvmLight.Command;
using Infrastructure;
using MaterialDesignThemes.Wpf;

namespace AppUI.ViewModels
{
    public class LibraryViewModel : ViewModelCustom
    {
        private IEnumerable<Sticker> _stickers;

        private RelayCommand<object> _editCommand;

        private RelayCommand<object> _deleteCommand;

        public LibraryViewModel(IFrameNavigationService navigationService) : base(navigationService)
        {
            Title = "Editor sticker library";
            LibraryInitialize();
        }

        public IEnumerable<Sticker> Stickers
        {
            get { return _stickers; }
            set { Set(nameof(Stickers), ref _stickers, value); }
        }

        public RelayCommand<object> EditCommand
        {
            get
            {
                return _editCommand ?? (_editCommand = new RelayCommand<object>(async (o) =>
                {
                    if (o is Sticker sticker)
                    {
                        var request = await DialogHelper.ShowDialog<EditDialogView, EditDialogViewModel, Sticker>(sticker);
                        if (request == null) return;
                        try
                        {
                            var file = PdfReadHelper.GetPageBytes(request.ToString());
                            sticker.File = file;
                            ThreadPool.QueueUserWorkItem(s =>
                            {
                                var service = ServiceLocator.Current.GetInstance<StickerService>();
                                service.Update(sticker);
                            });
                            DialogHelper.ShowInformerDialog(@"Changes has be saved", PackIconKind.ContentSave);
                        }
                        catch (PdfReadHelper.PdfDocumentIsEmptyException e)
                        {
                            DialogHelper.ShowInformerDialog(e.Message, PackIconKind.Error);
                            return;
                        }
                        catch (Exception e)
                        {
                            DialogHelper.ShowInformerDialog(e.Message, PackIconKind.Error);
                            return;
                        }
                    }
                }));
            }
        }

        public RelayCommand<object> DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new RelayCommand<object>(async (o) =>
                {
                    if (o is Sticker sticker)
                    {
                        bool request = await DialogHelper.ShowQuestionDialog($@"You wont delete select sticker?{Environment.NewLine}{sticker.Name}", PackIconKind.Delete);
                        if (request)
                        {
                            var service = ServiceLocator.Current.GetInstance<StickerService>();
                            service.Delete(sticker);
                            DialogHelper.ShowInformerDialog("Sticker has been deleted", PackIconKind.InfoOutline);
                            Stickers = service.GetAll();
                        }
                    }
                }));
            }
        }

        private void LibraryInitialize()
        {
            DialogHelper.ShowDialog(() =>
            {
                var service = ServiceLocator.Current.GetInstance<StickerService>();
                var list = service.GetAll();
                Stickers = list;
            }, $"Loading data{Environment.NewLine}Please wait...", "RootDialogHost");
        }
    }
}