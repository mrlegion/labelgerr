﻿using AppUI.Common.Services.NavigationService;
using GalaSoft.MvvmLight;

namespace AppUI.ViewModels
{
    public class SelectViewModel : ViewModelCustom
    {
        public SelectViewModel(IFrameNavigationService navigationService) : base(navigationService)
        {
        }
    }
}