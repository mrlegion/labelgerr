﻿using System.Windows;
using AppUI.Common.Services.NavigationService;
using GalaSoft.MvvmLight.Command;

namespace AppUI.ViewModels
{
    public class ShellWindowViewModel : ViewModelCustom
    {
        public ShellWindowViewModel(IFrameNavigationService navigationService) : base(navigationService)
        {
            Title = "Sticker library v0.1a";
        }

        private RelayCommand _onExitAppCommand;

        public RelayCommand OnExitAppCommand
        {
            get
            {
                return _onExitAppCommand ?? (_onExitAppCommand = new RelayCommand(() =>
                {
                    Application.Current.Shutdown();
                }));
            }
        }
    }
}