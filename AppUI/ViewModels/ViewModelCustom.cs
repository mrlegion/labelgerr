﻿using System;
using AppUI.Common.Services.NavigationService;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AppUI.ViewModels
{
    public class ViewModelCustom : ViewModelBase
    {
        private string _title;
        private RelayCommand<string> _navigateToCommand;
        private RelayCommand _goBackCommand;
        protected readonly IFrameNavigationService NavigationService;

        public ViewModelCustom(IFrameNavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        public string Title
        {
            get { return _title; }
            set { Set(nameof(Title), ref _title, value); }
        }

        public RelayCommand<string> NavigateToCommand
        {
            get
            {
                return _navigateToCommand ?? (_navigateToCommand = new RelayCommand<string>((page) =>
                {
                    if (string.IsNullOrEmpty(page) || string.IsNullOrWhiteSpace(page)) throw new ArgumentNullException(nameof(page), @"Page cannot be Null");
                    NavigationService.NavigateTo(page);
                }));
            }
        }

        public RelayCommand GoBackCommand
        {
            get
            {
                return _goBackCommand ?? (_goBackCommand = new RelayCommand(() =>
                {
                    NavigationService.GoBack();
                }));
            }
        }
    }
}