﻿using AppUI.Common.Services;
using AppUI.UserControls;
using AppUI.UserControls.AddToLibrary;
using AppUI.UserControls.Dialogs;
using AppUI.Views;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using CommonServiceLocator;
using Domain;

namespace AppUI.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<DomainModule>();

            // register components
            builder.RegisterModule<ViewModule>();
            builder.RegisterModule<ViewModelModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule<UserControlModule>();

            // register ioc
            var container = builder.Build();
            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));
        }

        public ShellWindowViewModel ShellWindow
        {
            get { return ServiceLocator.Current.GetInstance<ShellWindowViewModel>(); }
        }

        public LibraryViewModel Library
        {
            get { return ServiceLocator.Current.GetInstance<LibraryViewModel>(); }
        }

        public SelectViewModel Select
        {
            get { return ServiceLocator.Current.GetInstance<SelectViewModel>(); }
        }

        public AddToLibraryViewModel AddToLibrary
        {
            get { return ServiceLocator.Current.GetInstance<AddToLibraryViewModel>(); }
        }

        public GenerateStickerViewModel GenerateSticker
        {
            get { return ServiceLocator.Current.GetInstance<GenerateStickerViewModel>(); }
        }

        // dialogs

        public LoadDialogViewModel LoadDialog
        {
            get { return ServiceLocator.Current.GetInstance<LoadDialogViewModel>(); }
        }

        public EditDialogViewModel EditDialog
        {
            get { return ServiceLocator.Current.GetInstance<EditDialogViewModel>(); }
        }

        public DialogViewModel Dialog
        {
            get { return ServiceLocator.Current.GetInstance<DialogViewModel>(); }
        }

        public DialogGroupViewModel DialogGroupView
        {
            get { return ServiceLocator.Current.GetInstance<DialogGroupViewModel>(); }
        }

        public DialogGroupEditViewModel DialogGroupEdit
        {
            get { return ServiceLocator.Current.GetInstance<DialogGroupEditViewModel>(); }
        }

        public DialogAddStickerViewModel DialogAddSticker
        {
            get { return ServiceLocator.Current.GetInstance<DialogAddStickerViewModel>(); }
        }

        // user control

        public AddSingleViewModel AddSingle
        {
            get { return ServiceLocator.Current.GetInstance<AddSingleViewModel>(); }
        }

        public AddManyViewModel AddMany
        {
            get { return ServiceLocator.Current.GetInstance<AddManyViewModel>(); }
        }
    }
}