﻿using Autofac;

namespace AppUI.ViewModels
{
    public class ViewModelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShellWindowViewModel>();
            builder.RegisterType<LibraryViewModel>();
            builder.RegisterType<SelectViewModel>();
            builder.RegisterType<AddToLibraryViewModel>();
            builder.RegisterType<GenerateStickerViewModel>();
        }
    }
}