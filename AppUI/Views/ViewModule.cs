﻿using Autofac;

namespace AppUI.Views
{
    public class ViewModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShellWindow>();
            builder.RegisterType<LibraryView>();
            builder.RegisterType<SelectView>();
            builder.RegisterType<AddToLibraryView>();
            builder.RegisterType<GenerateStickerView>();
        }
    }
}