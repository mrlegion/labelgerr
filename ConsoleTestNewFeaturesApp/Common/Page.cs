using System;
using GerrLabelLibrary.Common;

namespace ConsoleTestNewFeaturesApp.Common
{
    public class Page
    {
        protected internal float Width;
        protected internal float Height;
        protected internal Orientation Orientation;

        public Page(float width, float height)
        {
            if (ValueLessOrEqualsZero(width))
                throw new SizeCanNotBeLessOrEqualsZeroExceprion($"{width}");
            
            if (ValueLessOrEqualsZero(height))
                throw new SizeCanNotBeLessOrEqualsZeroExceprion($"{height}");
            
            Width = width.ConvertMmToPt();
            Height = height.ConvertMmToPt();

            SetOrientation();
        }

        private void SetOrientation()
        {
            Orientation = (Width < Height) ? Orientation.Tall : Orientation.Wide;
        }

        private static bool ValueLessOrEqualsZero(float width)
        {
            return width <= 0;
        }

        public Page(Page page)
            : this(page.Width, page.Height) { }
        
        /// <summary>
        /// Получение ширины страницы в единицах Pt (Point)
        /// </summary>
        /// <returns>Ширина страницы в Pt (Point)</returns>
        public float GetWidth() => Width;

        /// <summary>
        /// Получение высоты страницы в еденицах Pt (Point)
        /// </summary>
        /// <returns></returns>
        public float GetHeight() => Height;

        /// <summary>
        /// Получение ширины страницы в милиметрах
        /// </summary>
        /// <returns>Ширина страницы в милиметрах</returns>
        public float GetWidthOnMm() => Width.ConvertPtToMm();

        /// <summary>
        /// Получение высоты страницы в милиметрах
        /// </summary>
        /// <returns></returns>
        public float GetHeightOnMm() => Height.ConvertPtToMm();

        /// <summary>
        /// Получение значения ориентации страницы
        /// </summary>
        public Orientation GetOrientation => Orientation;

        /// <summary>
        /// Установка новой ширины страницы
        /// </summary>
        /// <param name="width">Ширина страницы в мм</param>
        public void SetWidth(float width) => Width = width.ConvertMmToPt();

        /// <summary>
        /// Установка новой высоты страницы
        /// </summary>
        /// <param name="height">Высота страницы в мм</param>
        public void SetHeight(float height) => Height = height.ConvertMmToPt();

        /// <summary>
        /// Поворот страницы на 90 градусов
        /// </summary>
        public virtual void Rotation()
        {
            float w = this.Width;
            this.Width = this.Height;
            this.Height = w;

            SetOrientation();
        }

        #region Exceptions

        public class SizeCanNotBeLessOrEqualsZeroExceprion : Exception
        {
            public SizeCanNotBeLessOrEqualsZeroExceprion(string message) 
                : base($"Size of page can not be less or equal zero! Got: {message}") { }
        }

        #endregion
    }
}