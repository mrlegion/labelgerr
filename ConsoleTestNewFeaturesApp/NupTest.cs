﻿using System;
using System.Collections.Generic;
using ConsoleTestNewFeaturesApp.Common;
using GerrLabelLibrary.Common;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;

namespace ConsoleTestNewFeaturesApp
{
    public class NupTest
    {
        #region fields

        private PdfDocument _document;

        #endregion

        #region construct
        #endregion

        #region properties

        /// <summary>
        /// Получение групп для раскладки на печатный лист
        /// </summary>
        public int Group => Columns * Rows;

        /// <summary>
        /// Получение или установка количества строк при раскладке на печатный лист
        /// </summary>
        public int Rows { get; set; }

        /// <summary>
        /// Получение или установка значения количества колонок при раскладке на печатный лист
        /// </summary>
        public int Columns { get; set; }

        /// <summary>
        /// Получение или установка размеров страницы для печати, на который раскладывать
        /// </summary>
        public PrintPage PrintPage { get; set; }

        /// <summary>
        /// Получение или установка размеров страницы документа для раскладки
        /// </summary>
        public PrintPage DocumentPage { get; set; }

        /// <summary>
        /// Получение или установка точки привызки выравнивания на печатном листе
        /// </summary>
        public Pivot PivotOnPage { get; set; }

        /// <summary>
        /// Получение или установка ориентации раскладки 
        /// </summary>
        public Orientation LayoutOrientation { get; set; }

        #endregion

        #region private methods
        #endregion

        #region public methods

        /// <summary>
        /// Раскладка документа на печатный лист 
        /// </summary>
        /// <param name="source">Основной документ с ярлыками</param>
        /// <param name="file">Полный путь до файла сохранения разложенного файла</param>
        public void NupDocument(PdfDocument source, string file)
        {
            if (DocumentPage == null)
                throw new DocumentPageNullException();

            if (PrintPage == null)
                throw new PrintPageNullException();

            if (LayoutOrientation == Orientation.Null)
                throw new LayoutOrientationCanNotBeNullException();

            // проверка ориентации страницы и настройка переменной PrintPage
            if (LayoutOrientation != PrintPage.GetOrientation)
                PrintPage.Rotation();

            if (this.Columns == 0)
            {
                if (PrintPage.Width <= DocumentPage.Width)
                    Columns = 1;
                else if (PrintPage.Width > DocumentPage.Width)
                    Columns = (int)(PrintPage.Width / DocumentPage.Width);
            }

            if (this.Rows == 0)
            {
                if (PrintPage.Height <= DocumentPage.Height)
                    Rows = 1;
                else if (PrintPage.Height > DocumentPage.Height)
                    Rows = (int)(PrintPage.Height / DocumentPage.Height);
            }

            var pageCount = source.GetNumberOfPages() % Group > 0
                ? source.GetNumberOfPages() / Group + 1
                : source.GetNumberOfPages() / Group;

            _document = new PdfDocument(new PdfWriter(file));
            _document.SetDefaultPageSize(new PageSize(PrintPage.GetWidth(), PrintPage.GetHeight()));

            int count = 0;

            // Раскладка документа на печатный лист
            for (int i = 0; i < pageCount; i++)
            {
                PdfCanvas canvas = new PdfCanvas(_document.AddNewPage());

                for (int row = 0; row < Rows; row++)
                {
                    for (int col = 0; col < Columns; col++)
                    {
                        float x = DocumentPage.GetWidth() * col;
                        float y = (PrintPage.GetHeight() - DocumentPage.GetHeight()) - (DocumentPage.GetHeight() * row);

                        PdfFormXObject page = source.GetPage(++count).CopyAsFormXObject(_document);
                        canvas.AddXObject(page, x, y);
                    }
                }

            }

            source.Close();
            _document.Close();
        }

        /// <summary>
        /// Ассинхронная раскладка документа на печатный лист
        /// </summary>
        public void NupDocumetnAsync()
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region classes exceptions

        public sealed class DocumentPageNullException : Exception
        {
            public DocumentPageNullException()
                : base($"Document page is null!") { }
        }

        public sealed class PrintPageNullException : Exception
        {
            public PrintPageNullException()
                : base("Print page is null!") { }
        }

        public sealed class NUpCollectionNullOrEmptyException : Exception
        {
            public NUpCollectionNullOrEmptyException()
                : base("Collection for Nup empty or null") { }
        }

        public sealed class LayoutOrientationCanNotBeNullException : Exception
        {
            public LayoutOrientationCanNotBeNullException()
                : base("Layout otientation for Nup equals Null! Please check LayoutOrientation") { }
        }

        #endregion
    }
}