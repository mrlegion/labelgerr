﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTestNewFeaturesApp.Common;
using GerrLabelLibrary.Common;
using iText.Kernel.Pdf;

namespace ConsoleTestNewFeaturesApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string srcFile = @"C:\Users\boris\Desktop\gerr\300.pdf";
            string destFile = @"C:\Users\boris\Desktop\gerr\300 nup.pdf";
            FileInfo sourceFile = new FileInfo(srcFile);

            if (!sourceFile.Exists) throw new FileNotFoundException();

            PdfDocument sourceDocument = new PdfDocument(new PdfReader(sourceFile));

            NupTest nup = new NupTest()
            {
                Columns = 4,
                Rows = 4,
                PrintPage = PrintPage.A3.Rotation(),
                DocumentPage = new PrintPage(105f, 75f),
                LayoutOrientation = Orientation.Wide,
            };

            nup.NupDocument(sourceDocument, destFile);

            // Delay
            Console.WriteLine("Раскладка завершенна");
            Console.WriteLine("Для завершения нажмите любую кнопку...");
            Console.ReadKey();
        }

        
    }
}
