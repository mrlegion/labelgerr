﻿using System;
using System.Collections.Generic;

namespace ConsoleTestNewFeaturesApp
{
    public class Shuffle
    {
        private void ShuffleArray(ref List<char> list)
        {
            List<char> temp = new List<char>();

            int pages = list.Count / 16;

            for (int i = 0; i < pages; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    temp.Add(list[j * pages + i]);
                }
            }

            list = temp;
        }

        private List<char> CreateCopyArray(List<char> list, int count)
        {
            List<char> temp = new List<char>();

            foreach (var c in list)
            {
                for (int j = 0; j < count; j++)
                {
                    temp.Add(c);
                }
            }

            return temp;
        }

        private void AddRemainderItem(ref List<char> list, int empty)
        {
            for (int i = 0; i < empty; i++)
            {
                list.Add('e');
            }
        }

        private void ShowList(List<char> list)
        {
            int pages = list.Count / 16;
            int row = 0;
            for (int i = 0; i < pages; i++)
            {
                Console.WriteLine($"Page: {i + 1}");

                for (int j = 0; j < 16; j++)
                {
                    if (row == 4)
                    {
                        Console.WriteLine();
                        row = 0;
                    }

                    row++;
                    Console.Write(list[i * 16 + j] + "  ");
                }

                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}