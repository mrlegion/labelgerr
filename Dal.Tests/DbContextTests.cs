﻿using System.IO;
using System.Linq;
using Dal;
using Infrastructure;
using NUnit.Framework;

namespace StickerLibrary.Dal.Tests
{
    [TestFixture]
    public class DbContextTests
    {
        private StickerDbContext _dbContext;

        [SetUp]
        public void SetUp()
        {
            if (File.Exists(@".\db\Sticker.db"))
                File.Delete(@".\db\Sticker.db");
        }

        [Test]
        public void Construct_CreateDb_ExistDbFile()
        {
            // arrange
            _dbContext = new StickerDbContext();

            // act
            _dbContext.Stickers.Add(new Sticker("a3", new byte[1]));
            _dbContext.SaveChanges();

            // assert
            Assert.AreEqual(true, File.Exists(@".\db\Sticker.db"));
        }

        [Test]
        public void DbContext_AddItemToCollection_ExistRecordInDb()
        {
            //arrange
            var baseItem = new Sticker("a3", new byte[1]);
            _dbContext = new StickerDbContext();

            // act
            _dbContext.Stickers.Add(baseItem);
            _dbContext.SaveChanges();

            // assert
            var item = _dbContext.Stickers.FirstOrDefault(s => s.Id == 1);
            Assert.AreEqual(true, item != null && item.Name == baseItem.Name);
        }
    }
}
