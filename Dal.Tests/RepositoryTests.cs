﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Dal;
using Dal.Mehdime.DbScope.Implementations;
using Dal.Repository;
using Infrastructure;
using Moq;
using NUnit.Framework;

namespace StickerLibrary.Dal.Tests
{
    public class RepositoryTests
    {
        private IRepository<Sticker> _repository;
        private Sticker _sticker;

        [SetUp]
        public void SetUp()
        {
            _sticker = new Sticker("Test_format", new byte[1]);
            _repository = new StickerRepository(new AmbientDbContextLocator());
            using (var dbContextScope = new DbContextScopeFactory().Create())
            {
                dbContextScope.DbContexts.Get<StickerDbContext>().Stickers.Add(_sticker);
                dbContextScope.SaveChanges();
            }
        }

        [Test]
        public void Add_AddingItem_TrueReturned()
        {
            using (var dbContextScope = new DbContextScopeFactory().Create())
            {
                //arrange
                _repository = new StickerRepository(new AmbientDbContextLocator());

                //act
                _repository.Add(_sticker);
                dbContextScope.SaveChanges();

                //assert
                var item = dbContextScope.DbContexts.Get<StickerDbContext>().Stickers.FirstOrDefault(s => s.Name == _sticker.Name);
                Assert.AreEqual(true, item != null);
            }
        }

        [Test]
        public void Add_GetExceptionWhenAddingItem_ArgumentNullExceptionReturned()
        {
            // assert
            Assert.Catch<ArgumentNullException>(() => _repository.Add(null));
        }

        [Test]
        public void Update_UpdateItemInDb_TrueReturned()
        {
            using (var dbContextScope = new DbContextScopeFactory().Create())
            {
                //act
                _sticker.Name = "Updating_title";
                _repository.Update(_sticker);
                dbContextScope.SaveChanges();

                //assert
                var item = dbContextScope.DbContexts.Get<StickerDbContext>().Stickers.FirstOrDefault(s => s.Name == _sticker.Name);
                Assert.AreEqual(true, item != null && item.Name == "Updating_title");
            }
        }

        [Test]
        public void Update_GetExceptionWhenUpdate_ArgumentNullExceptionReturned()
        {
            // assert
            Assert.Catch<ArgumentNullException>(() => _repository.Update(null));
        }

        [Test]
        public void Delete_DeletedSelectEntity_NullReturned()
        {
            using (var dbContextScope = new DbContextScopeFactory().Create())
            {
                //act
                _repository.Delete(_sticker);
                dbContextScope.SaveChanges();

                //assert
                var item = dbContextScope.DbContexts.Get<StickerDbContext>().Stickers.FirstOrDefault(s => s.Name == _sticker.Name);
                Assert.AreEqual(null, item);
            }
            
        }

        [Test]
        public void Get_GetItemOnDb_StickerReturned()
        {
            using (new DbContextScopeFactory().CreateReadOnly())
            {
                //act
                var item = _repository.Get(1);

                //assert
                Assert.AreEqual(true, item != null && item.Name == _sticker.Name);
            }
        }

        [Test]
        public void Get_GetException_ArgumentOutOfRangeExceptionReturned()
        {
            //assert
            Assert.Catch<ArgumentOutOfRangeException>(() => _repository.Get(-1));
        }
    }
}