﻿using Autofac;
using Dal.Mehdime.DbScope.Implementations;
using Dal.Mehdime.DbScope.Interfaces;
using Dal.Repository;
using Infrastructure;

namespace Dal
{
    public class DalModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AmbientContextSuppressor>();
            builder.RegisterType<AmbientDbContextLocator>().As<IAmbientDbContextLocator>();
            builder.RegisterType<DbContextScope>().As<IDbContextScope>();
            builder.RegisterType<DbContextCollection>().As<IDbContextCollection>();
            builder.RegisterType<DbContextFactory>().As<IDbContextFactory>();
            builder.RegisterType<DbContextReadOnlyScope>().As<IDbContextReadOnlyScope>();
            builder.RegisterType<DbContextScopeFactory>().As<IDbContextScopeFactory>();
            builder.RegisterType<StickerRepository>().As<IRepository<Sticker>>();
            builder.RegisterType<PrintPageRepository>().As<IRepository<PrintPage>>();
            builder.RegisterType<StickerDbContext>();
        }
    }
}