﻿using System;
using System.Data.Entity;
using Dal.Mehdime.DbScope.Interfaces;

namespace Dal.Mehdime.DbScope.Implementations
{
    public class DbContextFactory : IDbContextFactory
    {
        public TDbContext CreateDbContext<TDbContext>() where TDbContext : DbContext
        {
            return Activator.CreateInstance<TDbContext>();
        }
    }
}