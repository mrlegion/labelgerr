﻿using System.Data.Entity;

namespace Dal.Mehdime.DbScope.Interfaces
{
    public interface IAmbientDbContextLocator
    {
        TDbContext Get<TDbContext>() where TDbContext : DbContext;
    }
}