﻿using System;
using System.Data.Entity;

namespace Dal.Mehdime.DbScope.Interfaces
{
    public interface IDbContextCollection : IDisposable
    {
        TDbContext Get<TDbContext>() where TDbContext : DbContext;
    }
}