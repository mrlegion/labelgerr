﻿using System.Data.Entity;

namespace Dal.Mehdime.DbScope.Interfaces
{
    public interface IDbContextFactory
    {
        TDbContext CreateDbContext<TDbContext>() where TDbContext : DbContext;
    }
}