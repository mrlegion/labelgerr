﻿using System;

namespace Dal.Mehdime.DbScope.Interfaces
{
    public interface IDbContextReadOnlyScope : IDisposable
    {
        IDbContextCollection DbContexts { get; }
    }
}