﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Infrastructure;

namespace Dal
{
    public class PrintPageFluentMap : EntityTypeConfiguration<PrintPage>
    {
        public PrintPageFluentMap()
        {
            ToTable("print_page").HasKey(p => p.Id);
            Property(p => p.Id)
                .HasColumnName("id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.Name)
                .HasColumnName("page_name")
                .HasColumnType("varchar")
                .HasMaxLength(100)
                .IsRequired();
            Property(p => p.Width)
                .HasColumnName("width_page")
                .HasColumnType("integer")
                .IsRequired();
            Property(p => p.Height)
                .HasColumnName("height_page")
                .HasColumnType("integer")
                .IsRequired();
        }
    }
}