﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Dal.Mehdime.DbScope.Interfaces;
using Infrastructure;

namespace Dal.Repository
{
    public class StickerRepository : IRepository<Sticker>
    {
        private readonly IAmbientDbContextLocator _locator; 

        private StickerDbContext DbContext
        {
            get
            {
                var context = _locator.Get<StickerDbContext>();
                if (context == null)
                    throw new InvalidOperationException("No ambient DbContext of type StickerDbContext found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");
                return context;
            }
        }

        public StickerRepository(IAmbientDbContextLocator locator)
        {
            _locator = locator ?? throw new ArgumentNullException(nameof(locator));
        }

        public Sticker Get(int id)
        {
            if (id < 0) throw new ArgumentOutOfRangeException(nameof(id), "Id can not be less zero");
            return DbContext.Stickers.FirstOrDefault(sticker => sticker.Id == id);
        }

        public IQueryable<Sticker> GetAll()
        {
            return DbContext.Stickers;
        }

        public void Add(Sticker item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item), "Cannot adding empty entity to database. Please check sending entity to method Add().");
            DbContext.Stickers.Add(item);
        }

        public void AddRange(IEnumerable<Sticker> entities)
        {
            var enumerable = entities.ToList();
            if (!enumerable.Any() || enumerable == null) throw new ArgumentNullException(nameof(entities), "Adding list cannot be empty. Check adding list parameter in method AddRange().");
            DbContext.Stickers.AddRange(enumerable);
        }

        public void Update(Sticker item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item), "Sended entity cannot be NULL. Please check sending entity to method Update().");
            DbContext.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Sticker item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item), "Sended entity cannot be NULL. Please check sending entity to method Delete().");
            DbContext.Entry(item).State = EntityState.Deleted;
        }
    }
}