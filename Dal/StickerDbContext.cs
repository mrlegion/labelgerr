﻿using System.Data.Entity;
using System.Reflection;
using Infrastructure;
using SQLite.CodeFirst;

namespace Dal
{
    public class StickerDbContext : DbContext
    {
        public StickerDbContext() : base("Default") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteDbInitialize = new SqliteCreateDatabaseIfNotExists<StickerDbContext>(modelBuilder);
            Database.SetInitializer(sqliteDbInitialize);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(StickerDbContext)));
        }

        public DbSet<Sticker> Stickers { get; set; }
        public DbSet<PrintPage> PagePrints { get; set; }
    }
}
