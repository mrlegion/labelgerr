﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Helpers;
using Domain.Nup;
using Domain.Shuffle;
using Infrastructure;

namespace Domain.Build
{
    class Builder : IBuilder
    {
        private IPageManager _manager;
        private IShuffler _shuffler;
        private INuper _nuper;

        public Builder(IPageManager manager, IShuffler shuffler, INuper nuper)
        {
            _manager = manager;
            _shuffler = shuffler;
            _nuper = nuper;
        }

        public bool Build(IEnumerable<Group> groups)
        {
            if (groups == null) throw new ArgumentNullException(nameof(groups));

            foreach (Group g in groups)
            {
                // create sticker base page array
                int[] stickerNormalDocument = new int[g.Stickers.Count()];
                // and fill
                for (int i = 0; i < stickerNormalDocument.Length; i++)
                    stickerNormalDocument[i] = i;

                // dublicated int array
                int[] stickerDublicatуDocument =
                    _manager.DublicatePages(stickerNormalDocument, g.Count, DublicateType.EachPage);

                // adding empty page (-1) to end array
                // need get length equals devided 16
                int remainded = stickerDublicatуDocument.Length % 16;
                stickerDublicatуDocument = _manager.AddEmptyPages(stickerDublicatуDocument, 16 - remainded);

                // get suffle document
                int[] stickerShufflepDocument = _shuffler.Shuffle(stickerDublicatуDocument, 16,
                    "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16");

                _nuper.Cols = 4;
                _nuper.Rows = 4;
                _nuper.AnchorOnPage = Anchor.TopLeft;
                _nuper.Orientation = Orientation.Wide;
            }

            return false;
        }
    }
}