﻿using System.Collections.Generic;
using Infrastructure;

namespace Domain.Build
{
    public interface IBuilder
    {
        bool Build(IEnumerable<Group> groups);
    }
}