﻿using Autofac;
using Dal;
using Domain.Helpers;
using Domain.Nup;
using Domain.Services;
using Domain.Shuffle;
using Infrastructure;

namespace Domain
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DalModule>();
            
            builder.RegisterType(typeof(StickerService));
            builder.RegisterType(typeof(PrintPageService));
            builder.RegisterType<IPageManager>().As<PageManager>();
            builder.RegisterType<INuper>().As<Nuper>();
            builder.RegisterType<IShuffler>().As<Shuffler>();
        }
    }
} 