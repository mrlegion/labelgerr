﻿using System.Collections.Generic;
using Infrastructure;

namespace Domain.Helpers
{
    public interface IPageManager
    {
        int[] AddEmptyPages(int[] source, int count, int emptyValue = -1);
        int[] DublicatePages(int[] source, int count, DublicateType dublicateType);
    }
}