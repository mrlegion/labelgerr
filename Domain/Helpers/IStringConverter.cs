﻿namespace Domain.Helpers
{
    public interface IStringConverter
    {
        int[] ConvertToIntArray(string value, char separator);
    }
}