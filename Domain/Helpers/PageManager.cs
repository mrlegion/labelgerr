﻿using System;
using System.Collections.Generic;
using Infrastructure;

namespace Domain.Helpers
{
    public class PageManager : IPageManager
    {
        public int[] AddEmptyPages(int[] source, int count, int emptyValue = -1)
        {
            var temp = new List<int>(source);
            for (int i = 0; i < count; i++)
                temp.Add(emptyValue);
            return temp.ToArray();
        }

        public int[] DublicatePages(int[] source, int count, DublicateType dublicateType)
        {
            switch (dublicateType)
            {
                case DublicateType.EachPage:
                    return EachPageDublicated(source, count);
                case DublicateType.GroupPage:
                    return GroupPageDublicated(source, count);
                case DublicateType.None:
                default:
                    throw new ArgumentOutOfRangeException(nameof(dublicateType), dublicateType, null);
            }
        }

        private static int[] EachPageDublicated(int[] source, int count)
        {
            var temp = new List<int>();
            foreach (var t in source)
                for (int j = 0; j < count; j++)
                    temp.Add(t);
            return temp.ToArray();
        }

        private int[] GroupPageDublicated(int[] source, int count)
        {
            var temp = new List<int>();
            for (int i = 0; i < count; i++) temp.AddRange(source);
            return temp.ToArray();
        }
    }
}