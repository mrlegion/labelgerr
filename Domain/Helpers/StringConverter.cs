﻿namespace Domain.Helpers
{
    public class StringConverter : IStringConverter
    {
        public int[] ConvertToIntArray(string value, char separator)
        {
            var trimmed = value.Trim();
            var splited = trimmed.Split(separator);
            int[] result = new int[splited.Length];
            for (int i = 0; i < splited.Length; i++)
                result[i] = int.Parse(splited[i]);
            return result;
        }
    }
}