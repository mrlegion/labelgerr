﻿using System.Collections.Generic;
using System.IO;
using Infrastructure;

namespace Domain.Nup
{
    public interface INuper
    {
        int Rows { get; set; }
        int Cols { get; set; }
        int Groups { get; }
        PrintPage PrintPage { get; set; }
        PrintPage DocumentPage { get; set; }
        Anchor AnchorOnPage { get; set; }
        Orientation Orientation { get; set; }
        void NupDocument(FileInfo file);
        void NupDocumentAsync(FileInfo file);
        void NupDocument(IEnumerable<Sticker> source, int[] pattern, string file);
    }
}