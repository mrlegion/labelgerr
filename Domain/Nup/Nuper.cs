﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using iText.Kernel.Geom;
using Infrastructure;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;
using Group = System.Text.RegularExpressions.Group;
using Path = System.IO.Path;

namespace Domain.Nup
{
    public class Nuper : INuper
    {
        private PdfDocument _sourceDocument;
        private PdfDocument _nupDocument;

        public int Rows { get; set; }

        public int Cols { get; set; }

        public int Groups
        {
            get { return Rows * Cols; }
        }

        public PrintPage PrintPage { get; set; }

        public PrintPage DocumentPage { get; set; }

        public Anchor AnchorOnPage { get; set; }

        public Orientation Orientation { get; set; }

        public Nuper(int rows, int cols, PrintPage printPage, PrintPage documentPage, Anchor anchorOnPage, Orientation orientation)
        {
            Rows = rows;
            Cols = cols;
            PrintPage = printPage;
            DocumentPage = documentPage;
            AnchorOnPage = anchorOnPage;
            Orientation = orientation;
        }

        public void NupDocument(FileInfo file)
        {
            // checked main options
            if (DocumentPage == null) throw new DocumentPageNullException();
            if (PrintPage == null) throw new PrintPageNullException();
            if (Orientation == Orientation.Null) throw new LayoutOrientationNullException();
            if (!file.Exists) throw new FileNotFoundException("Stickers file is not found");

            if (Cols == 0) AutoAssignmentCols();
            if (Rows == 0) AutoAssignmentRows();

            _sourceDocument = new PdfDocument(new PdfReader(file));
            int pageCount = _sourceDocument.GetNumberOfPages() % Groups > 0
                ? _sourceDocument.GetNumberOfPages() / Groups + 1
                : _sourceDocument.GetNumberOfPages() / Groups;

            _nupDocument = new PdfDocument(new PdfWriter(GetNamingNupFile(file)));
            _nupDocument.SetDefaultPageSize(new iText.Kernel.Geom.PageSize(PrintPage.Width, PrintPage.Height));

            int count = 0;
            for (int i = 0; i < pageCount; i++)
            {
                var canvas = new PdfCanvas(_nupDocument.AddNewPage());
                for (int row = 0; row < Rows; row++)
                {
                    for (int col = 0; col < Cols; col++)
                    {
                        float x = DocumentPage.Width * col;
                        float y = (PrintPage.Height - DocumentPage.Height) - (DocumentPage.Height * row);
                        PdfFormXObject page = _sourceDocument.GetPage(++count).CopyAsFormXObject(_nupDocument);
                        canvas.AddXObject(page, x, y);
                    }
                }
            }

            _sourceDocument.Close();
            _nupDocument.Close();
        }

        public void NupDocument(IEnumerable<Sticker> source, int[] pattern, string file)
        {
            // checked main options
            if (DocumentPage == null) throw new DocumentPageNullException();
            if (PrintPage == null) throw new PrintPageNullException();
            if (Orientation == Orientation.Null) throw new LayoutOrientationNullException();

            if (source == null) throw new ArgumentNullException(nameof(source));
            var stickers = source.ToList();
            if (stickers.Count == 0) throw new ArgumentNullException(nameof(source));

            if (string.IsNullOrEmpty(file) || string.IsNullOrWhiteSpace(file))
                throw new ArgumentNullException(nameof(file));

            var info = new FileInfo(file);
            if (!info.Exists) throw new FileNotFoundException("Stickers file is not found");

            if (Cols == 0) AutoAssignmentCols();
            if (Rows == 0) AutoAssignmentRows();

            int pages = pattern.Length % Groups > 0
                ? pattern.Length / Groups + 1
                : pattern.Length / Groups;

            using (var nupDoc = new PdfDocument(new PdfWriter(GetNamingNupFile(info))))
            {
                nupDoc.SetDefaultPageSize(new PageSize(PrintPage.Width, PrintPage.Height));
                for (int page = 0; page < pages; page++)
                {
                    var canvas = new PdfCanvas(nupDoc.AddNewPage());
                    for (int row = 1; row <= Rows; row++)
                    {
                        for (int col = 1; col <= Cols; col++)
                        {
                            float x = DocumentPage.Width * (col - 1);
                            float y = (PrintPage.Height - DocumentPage.Height) - (DocumentPage.Height * (row - 1));
                            int index = page * Groups + row * col;
                            PdfFormXObject obj = GetPdfFormXObject(stickers[index].File, nupDoc);
                            canvas.AddXObject(obj, x, y);
                        }
                    }
                }
            }
        }

        private PdfFormXObject GetPdfFormXObject(byte[] file, PdfDocument nupDoc)
        {
            using (var ms = new MemoryStream(file))
                using (var sticker = new PdfDocument(new PdfReader(ms)))
                    return sticker.GetFirstPage().CopyAsFormXObject(nupDoc);
        }

        private void AutoAssignmentRows()
        {
            if (PrintPage.Height <= DocumentPage.Height) Rows = 1;
            else if (PrintPage.Height > DocumentPage.Height) Rows = (int) (PrintPage.Height / DocumentPage.Height);
        }

        private void AutoAssignmentCols()
        {
            if (PrintPage.Width <= DocumentPage.Width) Cols = 1;
            else if (PrintPage.Width > DocumentPage.Width) Cols = (int) (PrintPage.Width / DocumentPage.Width);
        }

        private FileInfo GetNamingNupFile(FileInfo source)
        {
            string parrent = source.DirectoryName 
                             ?? throw new DirectoryNotFoundException("Source directory is not found");
            string name = Path.GetFileNameWithoutExtension(source.FullName);
            string file = Path.Combine(parrent, name + "_nup.pdf");
            return new FileInfo(file);
        }

        public void NupDocumentAsync(FileInfo file)
        {
            throw new System.NotImplementedException();
        }

        public class DocumentPageNullException : Exception
        {
            public DocumentPageNullException()
                : base("Document page size is null") { }
        }

        public class PrintPageNullException : Exception
        {
            public PrintPageNullException()
                : base("Print page size is null") { }
        }

        public class LayoutOrientationNullException : Exception
        {
            public LayoutOrientationNullException()
                : base("Orientation for nup is null") { }
        }
    }
}