﻿using Dal.Mehdime.DbScope.Interfaces;
using Dal.Repository;
using Infrastructure;

namespace Domain.Services
{
    public class PrintPageService : Service<PrintPage>
    {
        public PrintPageService(IDbContextScopeFactory dbContextScopeFactory, IRepository<PrintPage> repository) 
            : base(dbContextScopeFactory, repository)
        {}
    }
}