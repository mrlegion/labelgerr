﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dal.Mehdime.DbScope.Interfaces;
using Dal.Repository;

namespace Domain.Services
{
    public abstract class Service<T> : IService<T> where T : class
    {
        protected readonly IDbContextScopeFactory DbContextScopeFactory;
        protected readonly IRepository<T> Repository;

        protected Service(IDbContextScopeFactory dbContextScopeFactory, IRepository<T> repository)
        {
            DbContextScopeFactory =
                dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public virtual T Get(int id)
        {
            using (DbContextScopeFactory.CreateReadOnly())
            {
                return Repository.Get(id);
            }
        }

        public virtual IEnumerable<T> GetAll()
        {
            using (DbContextScopeFactory.CreateReadOnly())
            {
                return Repository.GetAll().ToList(); 
            }
        }

        public virtual void Add(T entity)
        {
            using (var dbContextScope = DbContextScopeFactory.Create())
            {
                Repository.Add(entity);
                dbContextScope.SaveChanges();
            }
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            using (var dbContextScope = DbContextScopeFactory.Create())
            {
                Repository.AddRange(entities);
                dbContextScope.SaveChanges();
            }
        }

        public virtual void Update(T entity)
        {
            using (var dbContextScope = DbContextScopeFactory.Create())
            {
                Repository.Update(entity);
                dbContextScope.SaveChanges();
            }
        }

        public virtual void Delete(T entity)
        {
            using (var dbContextScope = DbContextScopeFactory.Create())
            {
                Repository.Delete(entity);
                dbContextScope.SaveChanges();
            }
        }
    }
}