﻿using System;
using Dal.Mehdime.DbScope.Interfaces;
using Dal.Repository;
using Infrastructure;

namespace Domain.Services
{
    public class StickerService : Service<Sticker>
    {
        public StickerService(IDbContextScopeFactory dbContextScopeFactory, IRepository<Sticker> repository) 
            : base(dbContextScopeFactory, repository)
        { }
    }
}