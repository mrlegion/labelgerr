﻿
namespace Domain.Shuffle
{
    public interface IShuffler
    {
        int[] Shuffle(int[] source, int groups, string pattern);
    }
}