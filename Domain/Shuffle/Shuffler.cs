﻿using System;
using Domain.Helpers;
using Domain.Properties;

namespace Domain.Shuffle
{
    public class Shuffler : IShuffler
    {
        private int[] _source;
        private int[] _pattern;
        
        public int[] Shuffle(int[] source, int groups, string pattern)
        {
            if (source == null) throw new ArgumentNullException(nameof(source), Resources.Shuffler_throw_source_collection_null);

            _pattern = pattern.ConvertToIntArray(' ');
            if (groups != _pattern.Length) throw new GroupsAndPatternLengthNotEqualsException();

            int remainder = source.Length % groups;
            _source = new int[source.Length]; 

            // shuffle 
            int pages = source.Length / groups;
            for (int i = 0; i < pages; i++)
                for (int j = 0; j < groups; j++)
                {
                    int index = _pattern[j] + (i * groups); // calculate index for page
                    _source[i * groups + j] = source[index];
                }

            return _source;
        }

        public class GroupsAndPatternLengthNotEqualsException : Exception
        {
            public GroupsAndPatternLengthNotEqualsException() 
                : base(Resources.Groups_and_pattern_length_exception) { }
        }
    }
}