﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Domain.Split
{
    public class FileReadHelper
    {
        public static List<string> ReadFileToList(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException("Not found file with titles.", nameof(path));

            List<string> result = new List<string>();
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        result.Add(line);
                }

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}