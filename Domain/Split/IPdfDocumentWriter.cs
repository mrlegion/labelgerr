﻿using System.Collections.Generic;
using Infrastructure;

namespace Domain.Split
{
    public interface IPdfDocumentWriter
    {
        void SaveToFiles(IEnumerable<Sticker> stickers);
        void SaveToFile(Sticker sticker);
        bool SaveToFile(IEnumerable<Sticker> source, int[] savePattern, string path);
    }
}