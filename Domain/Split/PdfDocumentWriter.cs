﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Infrastructure;
using iText.Kernel.Pdf;

namespace Domain.Split
{
    public class PdfDocumentWriter : IPdfDocumentWriter
    {
        public void SaveToFiles(IEnumerable<Sticker> stickers)
        {
            foreach (Sticker sticker in stickers)
                SaveToFile(sticker);
        }

        public void SaveToFile(Sticker sticker)
        {
            string file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                "Pdf Files\\" + sticker.Name + ".pdf");
            FileInfo fi = new FileInfo(file);

            if (!Directory.Exists(fi.DirectoryName)) Directory.CreateDirectory(fi.DirectoryName);

            if (fi.Exists) fi.Delete();

            using (var document = new PdfDocument(new PdfWriter(fi)))
            {
                using (var ms = new MemoryStream(sticker.File))
                {
                    using (var sd = new PdfDocument(new PdfReader(ms)))
                    {
                        sd.CopyPagesTo(1, 1, document);
                    }
                }
            }
        }

        public bool SaveToFile(IEnumerable<Sticker> source, int[] savePattern, string path)
        {
            // check collection
            if (source == null) throw new ArgumentNullException(nameof(source));
            var stickers = source.ToList();
            if (stickers.Count == 0) throw new ArgumentException(nameof(source));

            // check path
            if (string.IsNullOrEmpty(path) || string.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException(nameof(path));
            var info = new FileInfo(path);
            // check file on exist
            // Todo: need create messanger sender to showing dialog when user select what need doing
            if (info.Exists) info.Delete();

            // Directory parrent check, if not exist need create he
            if (info.DirectoryName != null)
                if (!Directory.Exists(info.DirectoryName))
                    Directory.CreateDirectory(info.DirectoryName);

            int index = 0;
            using (var document = new PdfDocument(new PdfWriter(info)))
                foreach (int i in savePattern)
                    using (var ms = new MemoryStream(stickers[i].File))
                        using (var sticker = new PdfDocument(new PdfReader(ms)))
                            sticker.CopyPagesTo(1, ++index, document);

            return false;
        }
    }
}