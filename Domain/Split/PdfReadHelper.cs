﻿using System;
using System.Collections.Generic;
using System.IO;
using iText.Kernel.Pdf;

namespace Domain.Split
{
    public class PdfReadHelper
    {
        private static readonly Dictionary<int, byte[]> DocumentWithPages = new Dictionary<int, byte[]>();

        public static Dictionary<int, byte[]> GetPageByteDictionary(string file)
        {
            using (var sourceDocument = new PdfDocument(new PdfReader(file)))
            {
                for (int i = 0; i < sourceDocument.GetNumberOfPages(); i++)
                {
                    using (var ms = new MemoryStream())
                    {
                        var tempDocument = new PdfDocument(new PdfWriter(ms));
                        sourceDocument.CopyPagesTo(i + 1, i + 1, tempDocument);
                        tempDocument.Close();
                        DocumentWithPages.Add(i, ms.ToArray());
                    }
                }

                sourceDocument.Close();
            }

            return DocumentWithPages;
        }

        public static byte[] GetPageBytes(string file)
        {
            using (var sourceDocument = new PdfDocument(new PdfReader(file)))
            {
                if (sourceDocument.GetNumberOfPages() == 0) throw new PdfDocumentIsEmptyException();
                using (var ms = new MemoryStream())
                {
                    var tempDocument = new PdfDocument(new PdfWriter(ms));
                    sourceDocument.CopyPagesTo(1, 1, tempDocument);
                    tempDocument.Close();
                    return ms.ToArray();
                }
            }
        }

        public static byte[] GetPageBytesByNumber(string file, int page)
        {
            using (var document = new PdfDocument(new PdfReader(file)))
            {
                if (document.GetNumberOfPages() == 0) throw new PdfDocumentIsEmptyException();
                using (var ms = new MemoryStream())
                {
                    var tempDocument = new PdfDocument(new PdfWriter(ms));
                    document.CopyPagesTo(page, page, tempDocument);
                    tempDocument.Close();
                    return ms.ToArray();
                }
            }
        }

        public static int GetNumberOfPages(string file)
        {
            if (File.Exists(file))
                using (var document = new PdfDocument(new PdfReader(file)))
                    return document.GetNumberOfPages();
            throw new FileNotFoundException("Select file is not found! Please check your parameter", file);
        }

        public class PdfDocumentIsEmptyException : Exception
        {
            public PdfDocumentIsEmptyException() : base(@"In select pdf file not found pages or is empty!") { }
        }
    }
}