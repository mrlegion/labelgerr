﻿using System;
using System.Collections.Generic;
using iText.Layout.Element;
using Infrastructure;

namespace Domain.Split
{
    public class StickerFactory
    {
        public static List<Sticker> GetStickers(string title, string pdf)
        {
            List<string> titles = FileReadHelper.ReadFileToList(title);
            Dictionary<int, byte[]> stickers = PdfReadHelper.GetPageByteDictionary(pdf);

            if (titles.Count != stickers.Count)
                throw new ArgumentException();

            var stickerList = new List<Sticker>();

            for (int i = 0; i < titles.Count; i++)
                stickerList.Add(new Sticker(titles[i], stickers[i]));

            return stickerList;
        }
    }
}