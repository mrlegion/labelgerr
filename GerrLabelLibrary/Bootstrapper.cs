﻿using System.Windows;
using GerrLabelLibrary.Model.Implementation;
using GerrLabelLibrary.Model.Interfaces;
using GerrLabelLibrary.UserComponents;
using GerrLabelLibrary.ViewModel;
using GerrLabelLibrary.Views;
using Unity;

namespace GerrLabelLibrary
{
    public class Bootstrapper
    {
        private readonly IUnityContainer container;

        public Bootstrapper()
        {
            container = new UnityContainer();
        }

        public void Run()
        {
            RegisterComponents();
            CreateMainWindow();
            InitView();
        }

        private void InitView()
        {
            MainView main = container.Resolve<MainView>();

            //NewDesignView main = container.Resolve<NewDesignView>();
            main.DataContext = container.Resolve<MainViewModel>();
            Application.Current.MainWindow = main;
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            Application.Current.MainWindow.Show();
        }

        private void RegisterComponents()
        {
            container.RegisterType<ICollector, Collector>();
            container.RegisterType<IPdfShufflePage, PdfShufflePage>();
            container.RegisterType<IPdfBuilder, PdfBuilder>();
            container.RegisterType<INUper, NUper>();
        }

        private void RegisterUserComponent()
        {
            container.RegisterType<BrowseView>();
            container.RegisterType<BrowseViewModel>();
        }

        private void CreateMainWindow()
        {
            container.RegisterSingleton<MainView>();

            //container.RegisterSingleton<NewDesignView>();
            container.RegisterSingleton<MainViewModel>();

        }
    }
}