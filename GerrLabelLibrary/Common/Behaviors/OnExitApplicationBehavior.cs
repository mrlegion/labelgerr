﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace GerrLabelLibrary.Common.Behaviors
{
    /// <summary>
    /// Description this class
    /// </summary>
    public class OnExitApplicationBehavior : Behavior<Button>
    {
        #region Public methods

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Click += OnExitClickHandler;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Click -= OnExitClickHandler;
        }

        #endregion

        #region Private methods

        private void OnExitClickHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        #endregion
    }
}