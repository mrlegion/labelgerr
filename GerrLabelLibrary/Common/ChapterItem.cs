﻿using System.Collections;
using System.Collections.Generic;
using MyMVVMBase;

namespace GerrLabelLibrary.Common
{
    public class ChapterItem : BindableBase
    {
        #region private fields

        private int count;
        private int labelCount;
        private List<LabelItem> labelList;

        #endregion

        #region properties

        public int Count
        {
            get => count;
            set => SetProperty(ref count, value);
        }

        public int LabelCount
        {
            get => labelCount;
            set => SetProperty(ref labelCount, value);
        }

        public List<LabelItem> LabelList
        {
            get => labelList;
            set
            {
                SetProperty(ref labelList, value);
                LabelCount = LabelList.Count != LabelCount ? LabelList.Count : LabelCount;
            }
        }

        #endregion
    }
}