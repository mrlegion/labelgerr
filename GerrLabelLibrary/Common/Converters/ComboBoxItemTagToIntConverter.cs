﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace GerrLabelLibrary.Common.Converters
{
    public class ComboBoxItemTagToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ComboBoxItem cbi = (ComboBoxItem)value;
            if (cbi == null) return 0;
            return Int32.Parse(cbi.Tag.ToString());
        }
    }
}