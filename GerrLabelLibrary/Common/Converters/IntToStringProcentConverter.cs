﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace GerrLabelLibrary.Common.Converters
{
    public class IntToStringProcentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = value != null ? $"{(int) value}%" : "NuN";

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}