﻿using System;
using System.Collections.Generic;

namespace GerrLabelLibrary.Common
{
    public static class Helpers
    {
        public static bool CheckNull<T>(this IList<T> collection)
        {
            return collection == null ? true : false;
        }

        public static float ConvertMmToPt(this float value)
        {
            return (float) Math.Floor(value / 0.35278);
        }

        public static float ConvertPtToMm(this float value)
        {
            return (float) Math.Ceiling(value * 0.35278);
        }
        
        #region nested classes for exceptions

        public class ValueLessOrEqualsZeroException : Exception
        {
            public ValueLessOrEqualsZeroException(string message) : base(message)
            {
            }
        }

        #endregion
    }
}