﻿using System.IO;

namespace GerrLabelLibrary.Common
{
    public class LabelItem
    {
        public string Name { get; }
        public FileInfo Path { get; }
        public bool IsSelected { get; set; }

        public LabelItem(string name, FileInfo path)
        {
            Name = name;
            Path = path;
            IsSelected = false;
        }
    }
}