﻿namespace GerrLabelLibrary.Common
{
    /// <summary>
    /// Ориентация листа для раскладки
    /// </summary>
    public enum Orientation
    {
        /// <summary>
        /// Отсутствие определения ориентации страницы
        /// </summary>
        Null,

        /// <summary>
        /// Книжная ориентация раскладки
        /// </summary>
        Tall,

        /// <summary>
        /// Альбомная ориентация раскладки
        /// </summary>
        Wide
    }
}