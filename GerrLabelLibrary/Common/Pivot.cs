﻿namespace GerrLabelLibrary.Common
{
    /// <summary>
    /// Перечисление привязки объектов на печатном листе
    /// </summary>
    public enum Pivot
    {
        /// <summary>
        /// Выравнивание объекта левый верхний угол на листе
        /// </summary>
        TopLeft,

        /// <summary>
        /// Выравнивание объекта по центру сверху на листе
        /// </summary>
        TopCenter,

        /// <summary>
        /// Выравнивание объекта по вверхнему правому углу листа
        /// </summary>
        TopRight,

        /// <summary>
        /// Выравнивание объекта влево по центру листа
        /// </summary>
        LeftCenter,

        /// <summary>
        /// Выравнивание объекта по центру листа
        /// </summary>
        Center,

        /// <summary>
        /// Выравнивание объекта вправый по центру листа
        /// </summary>
        RightCenter,

        /// <summary>
        /// Выравнивание объекта по нижнему левому углу листа
        /// </summary>
        BottomLeft,

        /// <summary>
        /// Выравнивание объекта по центральной нижней границе листа
        /// </summary>
        BottomCenter,

        /// <summary>
        /// Выравнивание объекта по нижнему правому углу листа
        /// </summary>
        BottomRight
    }
}