﻿
namespace GerrLabelLibrary.Common
{
    public class PrintPage : Page
    {
        #region construct

        public PrintPage(float width, float height)
            : base(width, height) { }
        
        public PrintPage(Page page)
            : base (page) { }

        #endregion

        #region properties

        public static PrintPage A0          = new PrintPage(841f, 1189f);
        public static PrintPage A1          = new PrintPage(594f, 841f);
        public static PrintPage A2          = new PrintPage(420f, 594f);
        public static PrintPage A3          = new PrintPage(297f, 420f);
        public static PrintPage A4          = new PrintPage(210f, 297f);
        public static PrintPage A5          = new PrintPage(148f, 210f);
        public static PrintPage A6          = new PrintPage(105f, 148f);
        public static PrintPage A7          = new PrintPage(74f, 105f);
        public static PrintPage A8          = new PrintPage(52f, 74f);
        public static PrintPage A9          = new PrintPage(37f, 52f);
        public static PrintPage A10         = new PrintPage(26f, 37f);
        public static PrintPage B0          = new PrintPage(1000f, 1414f);
        public static PrintPage B1          = new PrintPage(707f, 1000f);
        public static PrintPage B2          = new PrintPage(500f, 707f);
        public static PrintPage B3          = new PrintPage(353f, 500f);
        public static PrintPage B4          = new PrintPage(250f, 353f);
        public static PrintPage B5          = new PrintPage(176f, 250f);
        public static PrintPage B6          = new PrintPage(125f, 176f);
        public static PrintPage B7          = new PrintPage(88f, 125f);
        public static PrintPage B8          = new PrintPage(62f, 88f);
        public static PrintPage B9          = new PrintPage(44f, 62f);
        public static PrintPage B10         = new PrintPage(31f, 44f);
        public static PrintPage LETTER      = new PrintPage(216f, 280f);
        public static PrintPage LEGAL       = new PrintPage(216f, 356f);
        public static PrintPage TABLOID     = new PrintPage(279f, 432f);
        public static PrintPage LEDGER      = new PrintPage(432f, 279f);
        public static PrintPage EXECUTIVE   = new PrintPage(185f, 267f);
        public static PrintPage Default     = PrintPage.A4;

        #endregion

        public new PrintPage Rotation()
        {
            return new PrintPage(this.Height, this.Width);
        }
    }
}