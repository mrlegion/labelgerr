﻿using System.IO;

namespace GerrLabelLibrary.Model.Common
{
    public class LabelItem
    {
        public string Name { get; }
        public FileInfo Path { get; }

        public LabelItem(FileInfo item)
        {
            if (!item.Exists) throw new FileNotFoundException(item.Name);

            Name = System.IO.Path.GetFileNameWithoutExtension(item.Name);
            Path = item;
        }
    }
}