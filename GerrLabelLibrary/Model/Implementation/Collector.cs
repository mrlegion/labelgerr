﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using GerrLabelLibrary.Common;
using GerrLabelLibrary.Model.Interfaces;

namespace GerrLabelLibrary.Model.Implementation
{
    public class Collector : ICollector
    {
        public Collector()
        {
        }

        public ObservableCollection<LabelItem> GetLabelCollection()
        {
            DirectoryInfo source = new DirectoryInfo("Source");
            ObservableCollection<LabelItem> items = new ObservableCollection<LabelItem>();

            if (!source.Exists) throw new DirectoryNotFoundException();

            FileInfo[] files = source.GetFiles();
            if (files.Length <= 0) throw new ArgumentException();
            foreach (FileInfo file in files)
            {
                if (!file.Exists) throw new FileNotFoundException();

                LabelItem item = new LabelItem(name: Path.GetFileNameWithoutExtension(file.Name), path: file);
                items.Add(item);
            }

            return items;
        }
    }
}