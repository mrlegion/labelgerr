﻿using System.Collections.ObjectModel;
using GerrLabelLibrary.Model.Common;

namespace GerrLabelLibrary.Model.Implementation
{
    public class LabelServices
    {
        private readonly ObservableCollection<LabelItem> labels;

        public LabelServices()
        {
            labels = new ObservableCollection<LabelItem>();
        }

        public ReadOnlyObservableCollection<LabelItem> Labels => new ReadOnlyObservableCollection<LabelItem>(labels);
    }
}