﻿using GerrLabelLibrary.Common;
using GerrLabelLibrary.Model.Interfaces;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using MyMVVMBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Unity;
using Path = System.IO.Path;

namespace GerrLabelLibrary.Model.Implementation
{
    /// <summary>
    ///     Предоставляет методы экземпляра класса для дублирования, перемешивания,
    ///     раскладки и сборки Pdf файлов этикеток для ИП Афонина.
    ///     Разработан для использования в программах ООО "Новоуральская типография"
    /// </summary>
    public class PdfBuilder : BindableBase, IPdfBuilder
    {
        #region Fields

        // components
        private readonly IPdfShufflePage _pdfShuffle;
        private readonly INUper _nuper;

        // local fields
        private PdfDocument _document;
        private PdfReader _reader;

        private bool _isWorking;
        private bool _useShuffle;
        private bool _useNUp;
        private DirectoryInfo _folder;

        private int _progress;
        private int _progressStep;
        private int _progressCount;

        #endregion

        #region Constract

        /// <summary>
        ///     Создание нового экземпляра класса <see cref="PdfBuilder" />
        /// </summary>
        /// <param name="container">Unity контейнер</param>
        public PdfBuilder(IUnityContainer container)
        {
            _pdfShuffle = container.Resolve<IPdfShufflePage>();
            _nuper = container.Resolve<INUper>();
        }

        public PdfBuilder() { }

        #endregion

        #region Properties

        /// <summary>
        ///     Получение значение занятости PdfBuilder работой
        /// </summary>
        public bool IsWorking
        {
            get => _isWorking;
            set => SetProperty(ref _isWorking, value);
        }

        /// <summary>
        ///     Получение или установка статуса использования опции перемешивания этикеток для расскладки
        ///     на формат а3 по 16 групп
        /// </summary>
        public bool UseShuffle
        {
            get => _useShuffle;
            set => SetProperty(ref _useShuffle, value);
        }

        /// <summary>
        ///     Получение или установка статуса использование опции
        ///     раскладки Pdf документа на формат а3 по 16 групп
        /// </summary>
        public bool UseNUp
        {
            get => _useNUp;
            set => SetProperty(ref _useNUp, value);
        }

        /// <summary>
        ///     Получение или установка директории сохранения Pdf файлов
        /// </summary>
        public DirectoryInfo Folder
        {
            get => _folder;
            set => SetProperty(ref _folder, value);
        }

        /// <summary>
        /// Получение значения процесса выполнения задачи
        /// </summary>
        public int Progress
        {
            get => _progress;
            set => SetProperty( ref _progress, value );
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Создание нового Pdf файла этикеток с дублированием этикеток и без перемешивания
        /// </summary>
        /// <param name="labels">Коллекция этикеток для заполнения Pdf файла</param>
        /// <param name="file">Путь до файла этикеток</param>
        /// <param name="quantity">Количество этикеток</param>
        private void PdfCreateDocument(List<LabelItem> labels, string file, int quantity)
        {
            // Инициализация нового документа
            PdfNewDocument(file);

            // Проверка на использования перемешивания
            if (UseShuffle)
            {
                _pdfShuffle.Shuffle(labels, quantity);
                labels = new List<LabelItem>(_pdfShuffle.ShuffleList);
            }

            // Перебор колекции этикеток
            foreach (var item in labels)
            {
                _progressCount++;

                if (item == null)
                {
                    _document.AddNewPage(
                                         new PageSize((float)(105 / 0.3528),
                                                       (float)(75 / 0.3528)));
                    continue;
                }

                _reader = new PdfReader(item.Path);
                var temp = new PdfDocument(_reader);

                // TODO: Стотит оптимизировать для более приятного вида
                if (UseShuffle)
                    temp.CopyPagesTo(1, 1, _document);
                else
                    for (var i = 0; i < quantity; i++)
                        temp.CopyPagesTo(1, 1, _document);

                temp.Close();
                _reader.Close();

                if ( _progressCount != _progressStep ) continue;

                Progress++;
                _progressCount = 0;
            }

            _document.Close();

            if (UseNUp)
            {
                _nuper.PrintPage = new PrintPage(420f, 297f);
                // TODO: Можно сделать проще, получать из переданного PdfDocument
                _nuper.DocumentPage = new PrintPage(105f, 75f);
                _nuper.Columns = 4;
                _nuper.Rows = 4;
                _nuper.LayoutOrientation = Orientation.Wide;

                string fName = Path.GetFileNameWithoutExtension(file) + " nup.pdf";
                string dName = Path.GetDirectoryName(file);

                if (string.IsNullOrEmpty(dName))
                    throw new DirectoryNotFoundException();

                string nFile = Path.Combine(dName, fName);

                _nuper.NupDocument(file, nFile);
            }
        }

        /// <summary>
        ///     Создание нового Pdf документа этикеток по указанному пути
        /// </summary>
        /// <param name="file">Полный путь к pdf файлу</param>
        private void PdfNewDocument(string file)
        {
            if (DocumentAlreadyOpen())
                _document.Close();

            _document = new PdfDocument(new PdfWriter(file));
            _document.SetDefaultPageSize(new PageSize(105, 75));
        }

        #endregion

        #region Check function

        /// <summary>
        /// Проверка на открытие документа PdfDocument
        /// </summary>
        /// <returns></returns>
        private bool DocumentAlreadyOpen()
        {
            return _document != null && !_document.IsClosed();
        }

        /// <summary>
        ///     Проверка количества на нуль или меньше
        /// </summary>
        /// <param name="value">количество</param>
        /// <returns></returns>
        private static bool LessOrEqualsZero(int value)
        {
            return value <= 0;
        }

        /// <summary>
        ///     Проверка списка на пустоту или null
        /// </summary>
        /// <param name="labels">Список для проверки</param>
        /// <returns></returns>
        private static bool ListNullOrEmpty(List<LabelItem> labels)
        {
            return labels == null || labels.Count == 0;
        }

        /// <summary>
        ///     Проверка списка на пустоту или null
        /// </summary>
        /// <param name="chapters">Список для проверки</param>
        /// <returns></returns>
        private static bool ListNullOrEmpty(List<ChapterItem> chapters)
        {
            return chapters == null || chapters.Count == 0;
        }

        /// <summary>
        ///     Проверка директории сохранения на пустоту и на нахождение
        /// </summary>
        /// <returns></returns>
        private bool FolderNullOrNotFound()
        {
            return Folder == null || !Folder.Exists;
        }

        #endregion

        /// <summary>
        /// Инициализация шага для расчета прогресса
        /// </summary>
        /// <param name="count">Общее количество этикеток</param>
        private void InitProgressStep( int count )
        {
            _progressStep = count / 100;
        }

        #region Public methods

        /// <summary>
        ///     Сборка Pdf файла этикеток
        /// </summary>
        /// <param name="labels">Колекция объектов выбранных этикеток типа <see cref="LabelItem" /></param>
        /// <param name="quantity">Количество копий этикеток</param>
        public void AssamblyPdf(List<LabelItem> labels, int quantity)
        {
            IsWorking = IsWorking ? IsWorking : !IsWorking;

            if ( _progressStep == 0 )
                InitProgressStep( labels.Count * quantity );

            if (FolderNullOrNotFound())
                throw new FolderNullOrNotFoundException(Folder?.FullName);

            if (ListNullOrEmpty(labels))
                throw new CollectionLabelsNullOrEmptyException();

            if (LessOrEqualsZero(quantity))
                throw new QuantityLessOrEquallyZeroException(quantity);

            string file = Path.Combine( Folder.FullName, $"{quantity}.pdf" );

            PdfCreateDocument(labels, file, quantity);

            IsWorking = !IsWorking;
        }

        /// <summary>
        /// Сборка Pdf файла этикеток
        /// </summary>
        /// <param name="chapters">Колекция объектов выбранных этикеток типа <see cref="LabelItem"/>,
        /// группированная по главам типа <see cref="ChapterItem"/></param>
        public void AssamblyPdf(List<ChapterItem> chapters)
        {
            if (ListNullOrEmpty(chapters))
                throw new CollectionLabelsNullOrEmptyException();

            InitProgressStep(chapters.Sum(item => item.LabelList.Count * item.Count));

            foreach (var chapter in chapters)
                AssamblyPdf(chapter.LabelList, chapter.Count);
        }

        // Todo создать систему проверки и перевода объекта в состояние работы
        /// <summary>
        ///     Ассинхронная сборка Pdf файла этикеток
        /// </summary>
        /// <param name="labels">Колекция объектов выбранных этикеток типа <see cref="LabelItem" /></param>
        /// <param name="quantity">Количество копий этикеток</param>
        public async void AssamblyPdfAsync(List<LabelItem> labels, int quantity) => 
            await Task.Factory.StartNew(() => AssamblyPdf(labels, quantity));

        /// <summary>
        /// Ассинхронная сборка Pdf файла этикеток
        /// </summary>
        /// <param name="chapters">Колекция объектов выбранных этикеток типа <see cref="LabelItem"/>,
        /// группированная по главам типа <see cref="ChapterItem"/></param>
        public async void AssamblyPdfAsync( List<ChapterItem> chapters ) =>
            await Task.Factory.StartNew( () => AssamblyPdf( chapters ) );

        #endregion

        #region Exception

        public class FolderNullOrNotFoundException : Exception
        {
            /// <summary>
            ///     Это исключение выбрасывается, если папка сохранения не найдена или не существует
            /// </summary>
            /// <param name="path">Директория, которая вызвала исключене</param>
            public FolderNullOrNotFoundException(string path)
                : base($"Select folder Null or not exist: [ {path} ]") { }
        }

        public class CollectionLabelsNullOrEmptyException : Exception
        {
            /// <summary>
            ///     Это исключение выбрасывается, если коллекция этикеток пустая или не существует
            /// </summary>
            public CollectionLabelsNullOrEmptyException()
                : base("Collections labels null or empty") { }
        }

        public class CollectionChaptersNullOrEmptyException : Exception
        {
            /// <summary>
            ///     Это исключение выбрасывается, если коллекция этикеток пустая или не существует
            /// </summary>
            public CollectionChaptersNullOrEmptyException()
                : base("Collections chapters null or empty") { }
        }

        public class QuantityLessOrEquallyZeroException : Exception
        {
            /// <summary>
            ///     Это исключение выбрасывается, если количество этикеток меньше или равно нулю
            /// </summary>
            /// <param name="quantity">Параметр количества, которое вызвало исключение</param>
            public QuantityLessOrEquallyZeroException(int quantity)
                : base($"Quantity can not be less or equals zero! Got: {quantity}") { }
        }

        #endregion
    }
}