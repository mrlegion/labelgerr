﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Documents;
using GerrLabelLibrary.Common;
using GerrLabelLibrary.Model.Interfaces;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using MyMVVMBase;
using Unity;
using Path = System.IO.Path;

namespace GerrLabelLibrary.Model.Implementation
{
    public class PdfMultiply : BindableBase, IPdfMultiply
    {
        private PdfDocument document;
        private PdfReader reader;
        private bool onWork = false;
        private bool useShuffle = true;
        private bool useNUp = false;
        private int progress = 0;

        // компонеты
        private readonly IPdfShufflePage pdfShufflePage;

        public PdfMultiply(IUnityContainer container)
        {
            pdfShufflePage = container.Resolve<IPdfShufflePage>();
        }

        public int Progress
        {
            get { return progress; }
            set { SetProperty(ref progress, value); }
        }
        /// <summary>
        /// Получение состояния работы сборщика файлов
        /// </summary>
        public bool OnWork
        {
            get { return onWork; }
            private set { SetProperty(ref onWork, value); }
        }

        /// <summary>
        /// Получение или установка параметра использования перемешивания при сборки файлов этикеток
        /// </summary>
        public bool UseShuffle
        {
            get { return useShuffle; }
            set { SetProperty(ref useShuffle, value); }
        }

        /// <summary>
        /// Получение или установка параметра использования раскладки этикеток на а3 формат
        /// </summary>
        public bool UseNUp
        {
            get { return useNUp; }
            set { SetProperty(ref useNUp, value); }
        }

        /// <summary>
        /// Сборка файлов этикеток
        /// </summary>
        /// <param name="list">Список этикеток</param>
        /// <param name="file">Наименование файла этикеток</param>
        /// <param name="count">Количество этикеток</param>
        public void AssemblyPdf(List<LabelItem> list, string file, int count)
        {
            OnWork = true;

            pdfShufflePage.Shuffle(list, count);

            int step = pdfShufflePage.ShuffleList.Count / 100;
            int c = 0;

            if (list == null || list.Count == 0)
                throw new ArgumentNullException();

            document = CreatePdfDocument(file);

            foreach (LabelItem labelItem in pdfShufflePage.ShuffleList)
            {
                if (c == step)
                {
                    c = 0;
                    Progress = Progress + 1;
                }

                if (labelItem == null)
                {
                    document.AddNewPage(
                        new PageSize((float)(105 / 0.3528),
                            (float)(75 / 0.3528)));
                    continue;
                }

                reader = new PdfReader(labelItem.Path);
                PdfDocument tmpd = new PdfDocument(reader);
                tmpd.CopyPagesTo(1, 1, document);
                tmpd.Close();
                reader.Close();

                c++;
            }

            //foreach (LabelItem item in list)
            //{
            //    AddPage(item, count);

            //    c++;

            //    if (c == step)
            //    {
            //        c = 0;
            //        progress++;
            //    }
            //}

            document.Close();

            OnWork = false;
        }

        public void AssamblyRangePdf(List<LabelItem> list, string file)
        {
            document = CreatePdfDocument(file);

            foreach (LabelItem labelItem in list)
            {
                if (labelItem == null)
                {
                    document.AddNewPage(
                        new PageSize((float)(105 / 0.3528),
                                     (float)(75 / 0.3528)));
                    continue;
                }

                reader = new PdfReader(labelItem.Path);
                PdfDocument tmpd = new PdfDocument(reader);
                tmpd.CopyPagesTo(1, 1, document);
                tmpd.Close();
                reader.Close();
            }

            document.Close();
        }

        /// <summary>
        /// Асинхронная сборка файлов этикеток
        /// </summary>
        /// <param name="list">Список этикеток</param>
        /// <param name="file">Наименование файла этикеток</param>
        /// <param name="count">Количество этикеток</param>
        public async void AssemblyPdfAsync(List<LabelItem> list, string file, int count)
        {
            await Task.Factory.StartNew(() => AssemblyPdf(list, file, count));
        }

        /// <summary>
        /// Многофайловая сборка этикеток
        /// </summary>
        /// <param name="collection">Список файлов с выбранными этикетками по количествам типа <see cref="ChapterItem"/></param>
        /// <param name="path">Путь до папки сохранения</param>
        public void MultiAssemblyPdf(List<ChapterItem> collection, string path)
        {
            OnWork = true;

            foreach (var chapterItem in collection)
            {
                pdfShufflePage.Shuffle(chapterItem.LabelList, chapterItem.Count);

                AssamblyRangePdf(pdfShufflePage.ShuffleList, Path.Combine(path, $"{chapterItem.Count}.pdf"));
            }

            OnWork = false;
        }

        /// <summary>
        /// Асинхронная многофайловая сборка этикеток
        /// </summary>
        /// <param name="collection">Список файлов с выбранными этикетками по количествам типа <see cref="ChapterItem"/></param>
        /// <param name="path">Путь до папки сохранения</param>
        public async void MultiAssemblyPdfAsync(List<ChapterItem> collection, string path)
        {
            await Task.Factory.StartNew(() => MultiAssemblyPdf(collection, path));
        }

        /// <summary>
        /// Создание нового документа Pdf
        /// </summary>
        /// <param name="file">Путь до нового файла</param>
        /// <returns><see cref="PdfDocument"/> объект</returns>
        private PdfDocument CreatePdfDocument(string file)
        {
            PdfDocument tempDocument = new PdfDocument(new PdfWriter(file));
            tempDocument.SetDefaultPageSize(new PageSize(105, 75));
            return tempDocument;
        }

        /// <summary>
        /// Добавление страниц в документ
        /// </summary>
        /// <param name="item">Объект этикетки типа<see cref="LabelItem"/> </param>
        /// <param name="count">Количество</param>
        private void AddPage(LabelItem item, int count)
        {
            reader = new PdfReader(item.Path);
            PdfDocument tmpd = new PdfDocument(reader);

            for (int i = 0; i < count; i++)
            {
                tmpd.CopyPagesTo(1, 1, document);
            }

            tmpd.Close();
            reader.Close();
        }
    }
}