﻿using System.Collections.Generic;
using GerrLabelLibrary.Common;
using GerrLabelLibrary.Model.Interfaces;

namespace GerrLabelLibrary.Model.Implementation
{
    public class PdfShufflePage : IPdfShufflePage
    {
        #region private field

        /// <summary>
        /// Количество групп на листе
        /// </summary>
        private int groups = 16;

        /// <summary>
        /// Перемещенный список этикеток
        /// </summary>
        private List<LabelItem> shuffleList;

        #endregion

        #region properties

        /// <summary>
        /// Получение перемешенного списка этикеток
        /// </summary>
        public List<LabelItem> ShuffleList => shuffleList ?? new List<LabelItem>();

        #endregion

        #region public methods

        /// <summary>
        /// Перемешивание списка этикеток
        /// </summary>
        /// <param name="label">Список этикеток</param>
        /// <param name="count">Количество копий</param>
        public void Shuffle(List<LabelItem> label, int count)
        {
            shuffleList = CreateCopyArray(label, count);

            int remainder = shuffleList.Count % groups;
            if (remainder > 0)
            {
                int empty = groups - remainder;
                AddRemainderItem(ref shuffleList, empty);
            }

            SetShuffleList(ref shuffleList);
        }

        #endregion

        #region private methods

        /// <summary>
        /// Дублирование этикеток с учетом копий
        /// </summary>
        /// <param name="list">Список этикеток</param>
        /// <param name="count">Количество копий</param>
        /// <returns></returns>
        private static List<LabelItem> CreateCopyArray(List<LabelItem> list, int count)
        {
            List<LabelItem> temp = new List<LabelItem>();

            foreach (var l in list)
            {
                for (int j = 0; j < count; j++)
                {
                    temp.Add(l);
                }
            }

            return temp;
        }

        /// <summary>
        /// Перемешевание списка этикеток с учетом раскладки на 16 групп
        /// </summary>
        /// <param name="list">Список этикеток</param>
        private void SetShuffleList(ref List<LabelItem> list)
        {
            List<LabelItem> temp = new List<LabelItem>();

            int pages = list.Count / 16;

            for (int i = 0; i < pages; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    temp.Add(list[j * pages + i]);
                }
            }

            list = temp;
        }

        /// <summary>
        /// Добавление пустых (null) объектов для выравнивания количества страниц
        /// </summary>
        /// <param name="list">Список этикеток</param>
        /// <param name="empty">Количество пустых страниц</param>
        private void AddRemainderItem(ref List<LabelItem> list, int empty)
        {
            for (int i = 0; i < empty; i++)
            {
                list.Add(null);
            }
        }

        #endregion
    }
}