﻿using System.Collections.ObjectModel;
using GerrLabelLibrary.Common;

namespace GerrLabelLibrary.Model.Interfaces
{
    public interface ICollector
    {
        ObservableCollection<LabelItem> GetLabelCollection();
    }
}