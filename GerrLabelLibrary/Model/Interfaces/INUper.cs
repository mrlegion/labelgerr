﻿using GerrLabelLibrary.Common;

namespace GerrLabelLibrary.Model.Interfaces
{
    public interface INUper
    {
        /// <summary>
        /// Получение или установка групп для раскладки на печатный лист
        /// </summary>
        int Group { get; }

        /// <summary>
        /// Получение или установка количества строк при раскладке на печатный лист
        /// </summary>
        int Rows { get; set; }

        /// <summary>
        /// Получение или установка значения количества колонок при раскладке на печатный лист
        /// </summary>
        int Columns { get; set; }

        /// <summary>
        /// Получение или установка размеров страницы для печати, на который раскладывать
        /// </summary>
        PrintPage PrintPage { get; set; }

        /// <summary>
        /// Получение или установка размеров страницы документа для раскладки
        /// </summary>
        PrintPage DocumentPage { get; set; }

        /// <summary>
        /// Получение или установка точки привызки выравнивания на печатном листе
        /// </summary>
        Pivot PivotOnPage { get; set; }

        /// <summary>
        /// Получение или установка ориентации раскладки 
        /// </summary>
        Orientation LayoutOrientation { get; set; }

        /// <summary>
        /// Раскладка документа на печатный лист 
        /// </summary>
        /// <param name="source">Основной документ с ярлыками</param>
        /// <param name="file">Полный путь до файла сохранения разложенного файла</param>
        void NupDocument(string source, string file);

        /// <summary>
        /// Ассинхронная раскладка документа на печатный лист
        /// </summary>
        /// <param name="source">Основной документ с ярлыками</param>
        /// <param name="file">Полный путь до файла сохранения разложенного файла</param>
        void NupDocumentAsync(string source, string file);
    }
}