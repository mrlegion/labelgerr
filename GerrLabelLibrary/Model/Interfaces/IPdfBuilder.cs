﻿using System.Collections.Generic;
using System.IO;
using GerrLabelLibrary.Common;

namespace GerrLabelLibrary.Model.Interfaces
{
    public interface IPdfBuilder
    {
        /// <summary>
        /// Получение значение занятости PdfBuilder работой
        /// </summary>
        bool IsWorking { get; }

        /// <summary>
        /// Получение или установка директории сохранения Pdf файлов
        /// </summary>
        DirectoryInfo Folder { get; set; }

        /// <summary>
        /// Получение или установка статуса использования опции перемешивания этикеток для расскладки
        /// на формат а3 по 16 групп
        /// </summary>
        bool UseShuffle { get; set; }

        /// <summary>
        /// Получение или установка статуса использование опции
        /// раскладки Pdf документа на формат а3 по 16 групп
        /// </summary>
        bool UseNUp { get; set; }

        /// <summary>
        /// Получение значения процесса выполнения задачи
        /// </summary>
        int Progress { get; }

        /// <summary>
        /// Сборка Pdf файла этикеток
        /// </summary>
        /// <param name="labels">Колекция объектов выбранных этикеток типа <see cref="LabelItem"/></param>
        /// <param name="quantity">Количество копий этикеток</param>
        void AssamblyPdf(List<LabelItem> labels, int quantity);

        /// <summary>
        /// Сборка Pdf файла этикеток
        /// </summary>
        /// <param name="chapters">Колекция объектов выбранных этикеток типа <see cref="LabelItem"/>,
        /// группированная по главам типа <see cref="ChapterItem"/></param>
        void AssamblyPdf(List<ChapterItem> chapters);

        /// <summary>
        /// Ассинхронная сборка Pdf файла этикеток
        /// </summary>
        /// <param name="labels">Колекция объектов выбранных этикеток типа <see cref="LabelItem"/></param>
        /// <param name="quantity">Количество копий этикеток</param>
        void AssamblyPdfAsync(List<LabelItem> labels, int quantity);

        /// <summary>
        /// Ассинхронная сборка Pdf файла этикеток
        /// </summary>
        /// <param name="chapters">Колекция объектов выбранных этикеток типа <see cref="LabelItem"/>,
        /// группированная по главам типа <see cref="ChapterItem"/></param>
        void AssamblyPdfAsync(List<ChapterItem> chapters);
    }
}