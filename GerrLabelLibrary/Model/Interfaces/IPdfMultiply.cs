﻿using System.Collections.Generic;
using GerrLabelLibrary.Common;

namespace GerrLabelLibrary.Model.Interfaces
{
    public interface IPdfMultiply
    {
        void AssemblyPdf(List<LabelItem> list, string file, int count);
        void AssemblyPdfAsync(List<LabelItem> list, string file, int count);
        void MultiAssemblyPdf(List<ChapterItem> collection, string path);
        void MultiAssemblyPdfAsync(List<ChapterItem> collection, string path);
        bool OnWork { get; }
        bool UseShuffle { get; set; }
        bool UseNUp { get; set; }
        int Progress { get; }
    }
}