﻿using System.Collections.Generic;
using GerrLabelLibrary.Common;

namespace GerrLabelLibrary.Model.Interfaces
{
    public interface IPdfShufflePage
    {
        List<LabelItem> ShuffleList { get; }

        void Shuffle(List<LabelItem> label, int count);
    }
}