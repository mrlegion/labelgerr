﻿using System.IO;
using Microsoft.WindowsAPICodePack.Dialogs;
using MyMVVMBase;

namespace GerrLabelLibrary.UserComponents
{
    public class BrowseViewModel : BindableBase
    {
        private DirectoryInfo parrent;
        private string selected;

        public BrowseViewModel()
        {
            var dialog = new CommonOpenFileDialog()
            {
                Multiselect = false,
                AllowNonFileSystemItems = true
            };

            SelectCommand = new DelegateCommand(() =>
            {
                dialog.Title = Title;
                dialog.IsFolderPicker = IsFolderPicker;

                if (Filters != null || Filters?.Length > 0 && !IsFolderPicker)
                {
                    foreach (string filter in Filters)
                    {
                        dialog.Filters.Add(new CommonFileDialogFilter($"Файл формата {filter}", filter));
                    }
                }

                if (parrent != null && parrent.Exists)
                {
                    dialog.InitialDirectory = parrent.FullName;
                }

                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    if (IsFolderPicker)
                    {
                        Directory = new DirectoryInfo(dialog.FileName);
                    }
                    else
                    {
                        File = new FileInfo(dialog.FileName);
                        Directory = File.Directory;
                    }

                    parrent = Directory?.Parent;
                    Selected = dialog.FileName;
                }

            });
        }

        public DelegateCommand SelectCommand { get; }

        public string Selected
        {
            get { return selected; }
            set { SetProperty(ref selected, value); }
        }

        public string Title { get; set; }

        public DirectoryInfo Directory { get; set; }

        public FileInfo File { get; set; }

        public bool IsFolderPicker { get; set; }

        public string[] Filters { private get; set; }
    }
}