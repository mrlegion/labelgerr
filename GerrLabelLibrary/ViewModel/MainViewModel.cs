﻿using GerrLabelLibrary.Common;
using GerrLabelLibrary.Model.Implementation;
using GerrLabelLibrary.Model.Interfaces;
using GerrLabelLibrary.UserComponents;
using GerrLabelLibrary.Views;
using MyMVVMBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Unity;

namespace GerrLabelLibrary.ViewModel
{
    public class MainViewModel : BindableBase
    {
        internal ObservableCollection<LabelItem> Items;
        private ObservableCollection<ChapterItem> _chapterItems;
        internal CollectionViewSource CViewSource { get; set; }
        private string _debugMessage;
        private string _searchText;
        private int _count;
        private bool _countIsNotNull;
        private ChapterItem _selectChapterItem;
        private bool _useManyFileSave = true;

        // Delete
        //private readonly IPdfMultiply _pdfMultiply;
        private readonly ListView _labelListView;

        private readonly IPdfBuilder _builder;

        public MainViewModel(IUnityContainer container)
        {
            ICollector collector = container.Resolve<ICollector>();

            _builder = container.Resolve<IPdfBuilder>();
            ((PdfBuilder) _builder).PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            Items = collector.GetLabelCollection();
            CViewSource = new CollectionViewSource() { Source = Items };
            CViewSource.Filter += NamingFilter;

            BrowseSaveFolderDataContext = container.Resolve<BrowseViewModel>();
            BrowseSaveFolderDataContext.Title = "Папка для сохранения файлов:";
            BrowseSaveFolderDataContext.IsFolderPicker = true;

            var window = container.Resolve<MainView>();
            _labelListView = window.StickerList;

            InitCommand();
        }

        private void NamingFilter(object sender, FilterEventArgs e)
        {
            LabelItem la = (LabelItem)e.Item;
            if (string.IsNullOrWhiteSpace(SearchText) || SearchText.Length == 0)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = (la.Name.IndexOf(SearchText, StringComparison.OrdinalIgnoreCase) >= 0);
            }
        }

        public ICollectionView ItemsFilters => CViewSource.View;

        // Delete Удалить это
        public string DebugMessage
        {
            get => _debugMessage;
            set => SetProperty(ref _debugMessage, value);
        }

        public string SearchText
        {
            get => _searchText;
            set
            {
                SetProperty(ref _searchText, value);
                OnFilterChanged();
            }
        }

        public ChapterItem SelectChapterItem
        {
            get { return _selectChapterItem; }
            set { SetProperty(ref _selectChapterItem, value); }
        }

        public int Count
        {
            get => _count;
            set
            {
                SetProperty(ref _count, value);
                CountIsNotNull = (Count > 0);
            }
        }

        public ObservableCollection<ChapterItem> ChapterItems
        {
            get => _chapterItems;
            set => SetProperty(ref _chapterItems, value);
        }

        public BrowseViewModel BrowseSaveFolderDataContext { get; set; }

        public bool CountIsNotNull
        {
            get => _countIsNotNull;
            set => SetProperty(ref _countIsNotNull, value);
        }

        private void OnFilterChanged()
        {
            CViewSource.View.Refresh();
        }

        private void UncheckAllItem()
        {
            Items.ToList().ForEach(item => item.IsSelected = false);
            _labelListView.SelectedItem = null;
        }

        public bool UseManyFileSave
        {
            get => _useManyFileSave;
            set => SetProperty(ref _useManyFileSave, value);
        }

        public bool IsWorking => _builder.IsWorking;

        public bool UseShuffle
        {
            get => _builder.UseShuffle;
            set => _builder.UseShuffle = value;
        }

        public bool UseNUp
        {
            get => _builder.UseNUp;
            set => _builder.UseNUp = value;
        }

        public List<LabelItem> SelectedItems
        {
            get { return (Items.Where(item => item.IsSelected)).ToList(); }
        }

        public int Progress => _builder.Progress;

        public DelegateCommand CombineCommand { get; private set; }
        public DelegateCommand ExitApplication { get; private set; }
        public DelegateCommand ClearListCommand { get; private set; }
        public DelegateCommand AddToDictinaryCommand { get; private set; }
        public DelegateCommand RemoveOnDictinaryCommand { get; private set; }

        private void InitCommand()
        {
            CombineCommand = new DelegateCommand(() =>
            {
                if (BrowseSaveFolderDataContext.Directory == null || !BrowseSaveFolderDataContext.Directory.Exists)
                {
                    MessageBox.Show("Невыбрана директория для сохранения", "Ошибка сборкии файла", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }

                _builder.Folder = BrowseSaveFolderDataContext.Directory;

                if (_useManyFileSave)
                {
                    if (ChapterItems.Count == 0)
                    {
                        MessageBox.Show("Нет выбранных элементов", "Ошибка сборкии файла", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return;
                    }
                    
                    _builder.AssamblyPdfAsync( ChapterItems.ToList() );

                    ChapterItems.Clear();
                }
                else
                {
                    if (SelectedItems.Count == 0)
                    {
                        MessageBox.Show("Нет выбранных элементов", "Ошибка сборкии файла", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return;
                    }

                    _builder.AssamblyPdfAsync(SelectedItems, Count);
                }

                Count = 0;
                UncheckAllItem();

            });

            ClearListCommand = new DelegateCommand(UncheckAllItem);

            AddToDictinaryCommand = new DelegateCommand(() =>
            {
                // если коллекция пустая, то создаем новую
                if (ChapterItems == null) ChapterItems = new ObservableCollection<ChapterItem>();

                // Если ничего не выбранно или ошибка при получении, то выдаем ошибку
                if (SelectedItems.Count == 0)
                {
                    MessageBox.Show("Нет выбранных элементов", "Ошибка сборкии файла", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }

                // Если это первый элемент, то добавляем его в коллекцию и завершаем команду
                if (ChapterItems.Count == 0)
                {
                    ChapterItems.Add(new ChapterItem() { Count = this.Count, LabelList = SelectedItems });
                    UncheckAllItem();
                    return;
                }

                List<LabelItem> selectedItems = new List<LabelItem>(SelectedItems);

                foreach (var item in ChapterItems)
                {
                    if (item.Count == this.Count)
                    {
                        foreach (LabelItem labelItem in item.LabelList)
                        {
                            foreach (LabelItem selectedItem in selectedItems)
                            {
                                if (labelItem.Name == selectedItem.Name)
                                {
                                    selectedItems.Remove(labelItem);
                                    break;
                                }
                            }
                        }
                        item.LabelList.AddRange(selectedItems);
                        UncheckAllItem();
                        return;
                    }
                }

                ChapterItems.Add(new ChapterItem() { Count = this.Count, LabelList = SelectedItems });

                UncheckAllItem();
            });

            RemoveOnDictinaryCommand = new DelegateCommand(() =>
            {
                if (SelectChapterItem == null) return;
                if (ChapterItems.Contains(SelectChapterItem))
                    ChapterItems.Remove(SelectChapterItem);
            });

            ExitApplication = new DelegateCommand(() =>
            {
                Application.Current.Shutdown(0);
            });
        }
    }


}