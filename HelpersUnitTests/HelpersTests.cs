﻿using System;
using GerrLabelLibrary.Common;
using NUnit.Framework;

namespace HelpersUnitTests
{
    [TestFixture]
    public class HelpersTests
    {
        [Test]
        public void ConvertValueToPtChecked()
        {
            float source = 297f;
            float result = source.ConvertMmToPt();
            Assert.AreEqual(841f, result);
        }

        [Test]
        public void ConvertValueToMmChecked()
        {
            float source = 841f;
            float result = source.ConvertPtToMm();
            Assert.AreEqual(297f, result);
        }
    }
}