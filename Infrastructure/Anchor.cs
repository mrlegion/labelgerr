﻿namespace Infrastructure
{
    public enum Anchor
    {
        None,
        TopLeft,
        TopCenter,
        TopRight,
        LeftCenter,
        Center,
        RightCenter,
        BottomLeft,
        BottomCenter,
        BottomRight
    }
}