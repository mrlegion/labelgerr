﻿using System;

namespace Infrastructure
{
    public static class ConvertHelper
    {
        private const float PtValue = 0.35278f;

        public static float ConvertToPt(this float value)
        {
            return (float) Math.Floor(value / PtValue);
        }

        public static float ConvertToMm(this float value)
        {
            return (float) Math.Floor(value * PtValue);
        }

        public static int[] ConvertToIntArray(this string value, char separator)
        {
            var trimmed = value.Trim();
            var splited = trimmed.Split(separator);
            int[] result = new int[splited.Length];
            for (int i = 0; i < splited.Length; i++)
                result[i] = int.Parse(splited[i]);
            return result;
        }
    }
}