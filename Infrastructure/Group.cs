﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight;

namespace Infrastructure
{
    public class Group : ViewModelBase, IEquatable<Group>
    {
        private string _name;

        public Group(string name, int count, IEnumerable<Sticker> stickers)
        {
            Name = name;
            Count = count;
            Stickers = stickers;
        }

        public Group()
        {
        }

        public string Name
        {
            get { return _name; }
            set { Set(nameof(Name), ref _name, value); }
        }

        private int _count;

        public int Count
        {
            get { return _count; }
            set { Set(nameof(Count), ref _count, value); }
        }

        private IEnumerable<Sticker> _stickers;

        public IEnumerable<Sticker> Stickers
        {
            get { return _stickers; }
            set { Set(nameof(Stickers), ref _stickers, value); }
        }

        public bool Equals(Group other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(_name, other._name) && _count == other._count;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Group) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_name != null ? _name.GetHashCode() : 0) * 397) ^ _count;
            }
        }
    }
}