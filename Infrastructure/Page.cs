﻿using System;

namespace Infrastructure
{
    public class Page
    {
        protected internal float Width;
        protected internal float Height;
        protected internal Orientation Orientation;

        public Page(float width, float height)
        {
            if (width <= 0)
                throw new ArgumentOutOfRangeException(nameof(width), "Width page cannot be less or equals zero!");
            if (height <= 0)
                throw new ArgumentOutOfRangeException(nameof(height), "Height page cannot be less or equals zero!");

            Width = width.ConvertToPt();
            Height = height.ConvertToPt();
            SetOrientation();
        }

        public Page(Page page) : this(page.Width, page.Height) { }

        public float GetWidth { get { return Width; } }

        public float GetHeight { get { return Height; } }

        public float GetWidthOnMm { get { return Width.ConvertToMm(); } }

        public float GetHeightOnMm { get { return Height.ConvertToMm(); } }

        public Orientation GetOrientation { get { return Orientation; } }

        public void SetWidth(float width)
        {
            Width = width.ConvertToPt();
        }

        public void SetHeight(float height)
        {
            Height = height.ConvertToPt();
        }

        public virtual void Rotation()
        {
            float width = Width;
            Width = Height;
            Height = width;
            SetOrientation();
        }

        private void SetOrientation()
        {
            Orientation = Width > Height ? Orientation.Wide : Orientation.Tall;
        }
    }
}
