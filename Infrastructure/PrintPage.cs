﻿namespace Infrastructure
{
    public class PrintPage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Orientation GetOrientation()
        {
            var index = Width.CompareTo(Height);
            return Width > Height
                ? Orientation.Wide
                : Width < Height
                    ? Orientation.Tall
                    : Orientation.Null;
        }
    }
}