﻿using System;

namespace Infrastructure
{
    public class Sticker : IEquatable<Sticker>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] File { get; set; }

        public Sticker(string name, byte[] file)
        {
            Name = name;
            File = file;
        }

        public Sticker()
        {
        }

        public bool Equals(Sticker other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && string.Equals(Name, other.Name) && Equals(File, other.File);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Sticker) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (File != null ? File.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
