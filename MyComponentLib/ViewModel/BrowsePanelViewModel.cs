﻿using System.IO;
using Microsoft.WindowsAPICodePack.Dialogs;
using MyMVVMBase;

namespace MyComponentLib.ViewModel
{
    public class BrowsePanelViewModel : BindableBase
    {
        private DirectoryInfo _parrent;
        private string _selected;
        private CommonOpenFileDialog _dialog;

        public BrowsePanelViewModel()
        {
            _dialog = new CommonOpenFileDialog()
            {
                Multiselect = false,
                AllowNonFileSystemItems = true
            };

            SelectCommand = new DelegateCommand(() =>
            {
                _dialog.Title = Title;
                _dialog.IsFolderPicker = IsFolderPicker;

                if (Filters.Length > 0 && !IsFolderPicker)
                {
                    foreach (string filter in Filters)
                    {
                        _dialog.Filters.Add(new CommonFileDialogFilter($"Файл формата {filter}", filter));
                    }
                }

                if (_parrent != null && _parrent.Exists)
                {
                    _dialog.InitialDirectory = _parrent.FullName;
                }

                if (_dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    if (IsFolderPicker)
                    {
                        Directory = new DirectoryInfo(_dialog.FileName);
                    }
                    else
                    {
                        File = new FileInfo(_dialog.FileName);
                        Directory = File.Directory;
                    }

                    _parrent = Directory?.Parent;
                    Selected = _dialog.FileName;
                }

            });
        }

        public DelegateCommand SelectCommand { get; }

        public string Selected
        {
            get { return _selected; }
            set { SetProperty(ref _selected, value); }
        }

        public string Title { get; set; }

        public DirectoryInfo Directory { get; set; }

        public FileInfo File { get; set; }

        public bool IsFolderPicker { get; set; }

        public string[] Filters { private get; set; }
    }
}