﻿using System.Collections.Generic;
using GerrLabelLibrary.Model.Implementation;
using GerrLabelLibrary.Model.Interfaces;
using NUnit.Framework;

namespace NUperUnitTests
{
    [TestFixture]
    public class NUperTests
    {
        private INUper _nUper;

        [ Test ]
        public void DefaultInitColumnPropertiesObjects()
        {
            _nUper = new NUper();
            _nUper.NupDocument(new List<int>());

            Assert.AreNotEqual(0, _nUper.Columns);
        }

        [Test]
        public void DefaultInitRowsPropertiesObjects()
        {
            _nUper = new NUper();
            _nUper.NupDocument(new List<int>());

            Assert.AreNotEqual(0, _nUper.Rows);
        }

        [TearDown]
        public void Destruct()
        {
            _nUper = null;
        }
    }
}
