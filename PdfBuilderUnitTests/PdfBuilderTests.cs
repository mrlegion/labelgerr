﻿using System.Collections.Generic;
using System.IO;
using GerrLabelLibrary.Common;
using GerrLabelLibrary.Model.Implementation;
using NUnit.Framework;

namespace PdfBuilderUnitTests
{
    [ TestFixture ]
    public class PdfBuilderTests
    {
        private PdfBuilder _builder;

        [ SetUp ]
        public void Init()
        {
            _builder = new PdfBuilder();
        }

        [ Test ]
        public void AssemblyPdfWithFolderException()
        {
            _builder.Folder = new DirectoryInfo( @"C:\Some Folder\Not Exist" );
            Assert.That( () => _builder.AssamblyPdf( new List<LabelItem>(), 1 ),
                         Throws.Exception.TypeOf<PdfBuilder.FolderNullOrNotFoundException>()
                               .With.Message
                               .EqualTo( "Select folder Null or not exist: [ C:\\Some Folder\\Not Exist ]" ) );
        }

        [ Test ]
        public void AssemblyPdfWithListException()
        {
            _builder.Folder = new DirectoryInfo( @"C:\Windows" );
            Assert.That( () => _builder.AssamblyPdf( new List<LabelItem>(), 100 ),
                         Throws.Exception.TypeOf<PdfBuilder.CollectionLabelsNullOrEmptyException>()
                               .With.Message.EqualTo( "Collections labels null or empty" ) );
        }

        [ Test ]
        public void QuantityLowZeroInAssamblyPdf_Exception()
        {
            _builder.Folder = new DirectoryInfo(@"C:\Windows");

            var tempList = new List<LabelItem>()
            {
                new LabelItem( "1", new FileInfo( @"C:\Path\To\Label" ) ),
                new LabelItem( "2", new FileInfo( @"C:\Path\To\Label" ) ),
                new LabelItem( "3", new FileInfo( @"C:\Path\To\Label" ) ),
                new LabelItem( "4", new FileInfo( @"C:\Path\To\Label" ) ),
                new LabelItem( "5", new FileInfo( @"C:\Path\To\Label" ) ),
            };

            Assert.That(() => _builder.AssamblyPdf(tempList, -10),
                        Throws.Exception.TypeOf<PdfBuilder.QuantityLessOrEquallyZeroException>()
                              .With.Message.EqualTo("Quantity can not be less or equals zero! Got: -10"));
        }
    }
}