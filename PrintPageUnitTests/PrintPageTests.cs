﻿using System;
using GerrLabelLibrary.Common;
using NUnit.Framework;

namespace PrintPageUnitTests
{
    [TestFixture]
    public class PrintPageTests
    {
        private PrintPage _page;
        
        [Test]
        public void CreateNewPageOnTemplates()
        {
            _page = PrintPage.A0;
            var w = _page.GetWidth();
            var h = _page.GetHeight();
            
            Assert.AreEqual(841f.ConvertMmToPt(), w);
            Assert.AreEqual(1189f.ConvertMmToPt(), h);
        }

        [Test]
        public void CreateNewPageOnCustomValues()
        {
            float w = 555f;
            float h = 756f;
            
            _page = new PrintPage(w, h);
            
            Assert.AreEqual(w.ConvertMmToPt(), _page.GetWidth());
            Assert.AreEqual(h.ConvertMmToPt(), _page.GetHeight());
        }

        [Test]
        public void CreateNewPageOnInitNewPageObjectValue()
        {
            float w = 297f;
            float h = 210f;
            
            _page = new PrintPage(new Page(w, h));
            
            Assert.AreEqual(w.ConvertMmToPt(), _page.GetWidth());
            Assert.AreEqual(h.ConvertMmToPt(), _page.GetHeight());
        }

        [Test]
        public void CheckExceptionCreateNewPage()
        {
            
            Assert.That(() => _page = new PrintPage(-5f, 150f),
                        Throws.Exception.TypeOf<Page.SizeCanNotBeLessOrEqualsZeroExceprion>()
                        .With.Message.EqualTo("Size of page can not be less or equal zero! Got: -5"));
        }

        [Test]
        public void RotationPageValidation()
        {
            _page = PrintPage.A4;
            _page.Rotation();

            Assert.AreEqual(297f.ConvertMmToPt(), _page.GetWidth());
            Assert.AreEqual(210f.ConvertMmToPt(), _page.GetHeight());
        }
        
        [TearDown]
        public void Clear()
        {
            _page = null;
        }
    }
}