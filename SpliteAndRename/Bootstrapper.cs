﻿using System.Net.Mime;
using System.Windows;
using MyComponentLib.View;
using MyComponentLib.ViewModel;
using SpliteAndRename.Model;
using SpliteAndRename.ViewModels;
using SpliteAndRename.Views;
using Unity;

namespace SpliteAndRename
{
    public class Bootstrapper
    {
        private readonly IUnityContainer _container;

        public Bootstrapper()
        {
            _container = new UnityContainer();
        }

        private void CreateMainView()
        {
            _container.RegisterSingleton<MainView>();
            _container.RegisterSingleton<MainViewModel>();
        }

        private void InitMainView()
        {
            MainView main = _container.Resolve<MainView>();
            main.DataContext = _container.Resolve<MainViewModel>();
            Application.Current.MainWindow = main;
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            Application.Current.MainWindow.Show();
        }

        private void RegisterUserComponent()
        {
            _container.RegisterType<BrowsePanelView>();
            _container.RegisterType<BrowsePanelViewModel>();
        }

        private void RegisterComponent()
        {
            _container.RegisterType<Spliter>();
        }

        public void Run()
        {
            CreateMainView();
            InitMainView();
            RegisterUserComponent();
            RegisterComponent();
        }
    }
}