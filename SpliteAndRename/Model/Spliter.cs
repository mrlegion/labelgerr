﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Documents;
using iText.Kernel.Pdf;

namespace SpliteAndRename.Model
{
    public class Spliter
    {
        private DirectoryInfo _fileDirectory;
        private FileInfo _fileSource;
        private List<string> _namingPosition;
        private PdfReader _reader;
        private PdfDocument _document;
        private PdfWriter _writer;

        public Spliter()
        {
        }

        public FileInfo SourceFile
        {
            set
            {
                if (!value.Exists) throw new FileNotFoundException(value.Name);

                _fileSource = value;
                _fileDirectory = _fileSource.Directory;
            }
        }

        public FileInfo NamingFile { private get; set; }

        private void GetNamingArray()
        {
            if (NamingFile == null || !NamingFile.Exists) throw new FileNotFoundException(NamingFile?.Name);

            if (_namingPosition == null)
                _namingPosition = new List<string>();

            if (_namingPosition != null && _namingPosition.Count > 0)
                _namingPosition.Clear();

            StreamReader reader = new StreamReader(NamingFile.Open(FileMode.Open, FileAccess.Read));
            string line = reader.ReadLine();
            while (line != null)
            {
                _namingPosition.Add(line);
                line = reader.ReadLine();
            }

            reader.Close();
        }

        private void CreateDirectory()
        {
            if (!_fileDirectory.Exists) throw new DirectoryNotFoundException();

            _fileDirectory.CreateSubdirectory("Source");
        }

        private void SplitePdfDoc()
        {
            if (!_fileSource.Exists) throw new FileNotFoundException();

            CreateDirectory();

            _reader = new PdfReader(_fileSource);
            _document = new PdfDocument(_reader);

            for (int i = 0; i < _document.GetNumberOfPages(); i++)
            {
                FileInfo fi = new FileInfo($"{_fileDirectory.FullName}\\Source\\{_namingPosition[i]}.pdf");
                _writer = new PdfWriter(fi);
                PdfDocument newDoc = new PdfDocument(_writer);

                _document.CopyPagesTo(i + 1, i + 1, newDoc);

                newDoc.Close();
                _writer.Close();
            }
        }

        public void Split()
        {
            GetNamingArray();
            SplitePdfDoc();
        }
    }
}