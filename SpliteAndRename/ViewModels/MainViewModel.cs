﻿using System.Windows;
using MyComponentLib.ViewModel;
using MyMVVMBase;
using SpliteAndRename.Model;
using Unity;

namespace SpliteAndRename.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private readonly Spliter _spliter;

        public MainViewModel(IUnityContainer container)
        {
            BrowseFileDataContext = container.Resolve<BrowsePanelViewModel>();
            BrowseFileDataContext.IsFolderPicker = false;
            BrowseFileDataContext.Title = "Выбор файла с ярлыками";
            BrowseFileDataContext.Filters = new[] { "pdf" };

            BrowseNamingFileDataContext = container.Resolve<BrowsePanelViewModel>();
            BrowseNamingFileDataContext.IsFolderPicker = false;
            BrowseNamingFileDataContext.Title = "Выбор файла с наименованиеями ярлыков";
            BrowseNamingFileDataContext.Filters = new[] { "txt" };

            _spliter = container.Resolve<Spliter>();

            InitCommand();
        }

        public BrowsePanelViewModel BrowseFileDataContext { get; }

        public BrowsePanelViewModel BrowseNamingFileDataContext { get; }

        public DelegateCommand ExitCommand { get; private set; }

        public DelegateCommand SplitCommand { get; private set; }

        private void InitCommand()
        {
            SplitCommand = new DelegateCommand(() =>
            {
                _spliter.NamingFile = BrowseNamingFileDataContext.File;
                _spliter.SourceFile = BrowseFileDataContext.File;
                _spliter.Split();
                MessageBox.Show("Complite!");
            });

            ExitCommand = new DelegateCommand(() => Application.Current.Shutdown());
        }
    }
}