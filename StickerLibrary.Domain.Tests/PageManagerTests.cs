﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;

namespace StickerLibrary.Domain.Tests
{
    [TestFixture]
    public class PageManagerTests
    {
        private IPageManager<int> _manager;
        private List<int> _pages;

        [SetUp]
        public void SetUp()
        {
            _pages = new List<int>() { 1, 2, 3, 4, 5 };
            _manager = new PageManager<int>();
        }

        [Test]
        public void AddEmptyPages_AddingDefaultObjectToEndPages_TrueReturned()
        {
            int count = 6;
            var returned = (List<int>) _manager.AddEmptyPage(_pages, count);
            Assert.AreEqual(_pages.Count + count, returned.Count, "Count elements in returned collection are not equals expected value");
            returned.RemoveRange(0, _pages.Count);
            Assert.AreEqual(count, returned.Count, "After remove base page count on returned collection count page not equals addnig count pages");
            foreach (int i in returned)
                Assert.AreEqual(0, i, "Adding empty element are not eqauls expected value");
        }

        [Test]
        public void DublicatePages_DublicatedPagesInCollections_TrueReturned()
        {
            int count = 10;
            var returned = (List<int>) _manager.DublicatePages(_pages, count);

            Assert.AreEqual(count * _pages.Count, returned.Count, "Count page in returned collection are not equals on expected value");
            for (int i = 0; i < _pages.Count; i++)
                for (int j = 0; j < count; j++)
                    Assert.AreEqual(_pages[i], returned[j + i * count], "Page value is not equals expected value");
        }
    }
}
