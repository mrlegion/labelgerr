﻿using System.Collections.Generic;
using System.Linq;
using Dal;
using Dal.Mehdime.DbScope.Implementations;
using Dal.Repository;
using Domain.Services;
using Infrastructure;
using NUnit.Framework;

namespace StickerLibrary.Domain.Tests
{
    public class StickerServiceTests
    {
        private StickerService _service;
        private Sticker _sticker;

        [SetUp]
        public void SetUp()
        {
            _service = new StickerService(new DbContextScopeFactory(), new StickerRepository(new AmbientDbContextLocator()));
            _sticker = new Sticker("Test sticker", new byte[1]);
        }

        [Test]
        public void Add_AddingEntity_TrueReturned()
        {
            _service.Add(_sticker);

            using (var dbContext = new DbContextScopeFactory().CreateReadOnly())
                Assert.AreEqual(true,
                    dbContext.DbContexts.Get<StickerDbContext>().Stickers
                        .FirstOrDefault(sticker => sticker.Name == _sticker.Name) != null);
        }

        [Test]
        public void Get_GetItemOnDbContext_ItemReturned()
        {
            using (var dbContext = new DbContextScopeFactory().Create())
            {
                dbContext.DbContexts.Get<StickerDbContext>().Stickers.Add(_sticker);
                dbContext.SaveChanges();
            }

            var item = _service.Get(1);

            bool result = item != null &&
                          item.Name == _sticker.Name &&
                          item.File.Length == _sticker.File.Length;
            Assert.AreEqual(true, result);
        }

        [Test]
        public void GetAll_GetListItemOnDb_IQuarableReturns()
        {
            using (var dbContext = new DbContextScopeFactory().Create())
            {
                var list = new List<Sticker>()
                {
                    new Sticker("first", new byte[1]),
                    new Sticker("second", new byte[1]),
                    new Sticker("three", new byte[1]),
                    new Sticker("four", new byte[1]),
                    new Sticker("five", new byte[1]),
                };

                dbContext.DbContexts.Get<StickerDbContext>().Stickers.AddRange(list);
                dbContext.SaveChanges();

                // operation
                var itemList = _service.GetAll();

                // assert
                if (itemList == null) Assert.Fail("list is null");
                Assert.AreEqual(5, itemList.Count());
            }
        }

        [Test]
        public void Update_UpdatingSelectEntity_TrueReturned()
        {
            using (var dbContext = new DbContextScopeFactory().Create())
            {
                dbContext.DbContexts.Get<StickerDbContext>().Stickers.Add(_sticker);
                dbContext.SaveChanges();
                
                // operation
                _sticker.Name = "Updating";
                _service.Update(_sticker);

                // assert
                var item = dbContext.DbContexts.Get<StickerDbContext>().Stickers.FirstOrDefault(sticker => sticker.Id == 1);
                if (item == null) Assert.Fail("returned item is null");
                Assert.AreEqual("Updating", item.Name);
            }
        }

        [Test]
        public void Delete_DeletedEntityOnDb_FalseReturned()
        {
            using (var dbContextScope = new DbContextScopeFactory().Create())
            {
                dbContextScope.DbContexts.Get<StickerDbContext>().Stickers.Add(_sticker);
                dbContextScope.SaveChanges();
            }

            // operation
            _service.Delete(_sticker);

            // assert
            using (var dbContextScope = new DbContextScopeFactory().CreateReadOnly())
                Assert.AreEqual(false, dbContextScope.DbContexts.Get<StickerDbContext>().Stickers.Any());
        }

        [TearDown]
        public void Clear()
        {
            _service = null;
            _sticker = null;
        }
    }
}
