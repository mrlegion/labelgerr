﻿using NUnit.Framework;
using Domain.Helpers;

namespace StickerLibrary.Domain.Tests
{
    [TestFixture]
    public class StringConverterTests
    {
        private IStringConverter _converter;
        private string _pattern;

        [SetUp]
        public void SetUpTests()
        {
            _pattern = "1 3 5 7 4 2 8 6";
            _converter = new StringConverter();
        }

        [Test]
        public void ConvertToIntArray_ConvertingStringToIntArray_IntArrayReturned()
        {
            int[] returned = _converter.ConvertToIntArray(_pattern, ' ');

            int[] equal = new[] {1, 3, 5, 7, 4, 2, 8, 6};

            if (returned.Length != equal.Length)
                Assert.Fail("Length returned array not equals expected length");

            for (int i = 0; i < returned.Length; i++)
                if (returned[i] != equal[i])
                    Assert.Fail("Not equals value in arrays");

            Assert.Pass("Test is passed");
        }
    }
}
